<?php
class Admin extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
 
    }

    public function index()
    {
        if($this->session->userdata('status')=='admin'){
            $title = 'Home';
            $bc = array(
                'id'     => $this->session->userdata('id'),
                'status' => $this->session->userdata('status'),
                'username' => $this->session->userdata('username'),
                'judul'=> $title,
            );
            
            $this->load->view('header',$bc);              
            // $this->load->view('home'); 
            // $this->load->view('header');
            $this->load->view('side_menu');
            $this->load->view('master_template');        
            $this->load->view('footer');


        }
       
    }

    
}