<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {
    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if($this->session->userdata('id')==''){
            redirect('Web');
        }
        
    }

	public function index()
	{
		$this->load->model('Mymodel');  
        $title = 'Inventori Keluar';

        $data = array('judul'=> $title);      
       
        $this->load->view('Adm/v_formcetak',$data);
        
	}

	public function cetak_lap(){
		$this->load->model('Mymodel');
        $pil = $this->input->post('radio-inline');
        $title = 'Inventori Keluar';

        if (isset($pil)&&$pil=='All'){
           $data['lap'] = $this->db->query("SELECT * FROM vw_inventori_keluar Order by tgl_kl");
        }else{           
            $tglawal = date('Y-m-d', strtotime($this->input->post('nm_tgl1')));
            $tglakhir = date('Y-m-d', strtotime($this->input->post('nm_tgl2')));


            $data['lap'] = $this->db->query("SELECT * FROM vw_inventori_keluar where tgl_kl between '$tglawal' and '$tglakhir'");
        }
   
        $data['judul'] = $title;
        $this->load->view('Adm/v_laporan',$data);
	}


    public function form_ctk_po()
    {
        $this->load->model('Mymodel');  
        $title = 'Pre Order';

        $data = array('judul'=> $title);      
       
        $this->load->view('Adm/v_form_cetak_po',$data);
        
    }

    public function cetak_pre_order(){
        $this->load->model('Mymodel');
        $pil = $this->input->post('radio-inline');
        $title = 'Pre Order';

        if (isset($pil)&&$pil=='All'){
           $data['lap'] = $this->db->query("SELECT * FROM vw_po Order by tgl_po");
        }else{           
            $tglawal = date('Y-m-d', strtotime($this->input->post('nm_tgl1')));
            $tglakhir = date('Y-m-d', strtotime($this->input->post('nm_tgl2')));


            $data['lap'] = $this->db->query("SELECT * FROM vw_inventori_keluar where tgl_po between '$tglawal' and '$tglakhir'");
        }
   
        $data['judul'] = $title;
        $this->load->view('Adm/v_laporan_po',$data);
    }

    
}
