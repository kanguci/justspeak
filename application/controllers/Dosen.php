<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {
    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if($this->session->userdata('id')==''){
            redirect('Web');
        }
        
    }

	public function index()
	{
        var_dump($this->session->userdata('logged_in')) ;  
		$this->load->model('Mymodel');
        $dt_dsn = $this->Mymodel->GetTabel('tbl_dosen');        
        
        $title = 'Dosen';
        $data = array('dtdsn' => $dt_dsn,'judul'=> $title); //judul untuk dipanggil ke view
        //$this->load->view('header');
        $this->load->view('Adm/v_dosen',$data);
        
	}

	public function simpan(){
		$this->load->model('Mymodel');
       
        if(isset($_POST['BtnSimpan'])){

                $kd = $this->input->post("NmNidn");
                $cek = $this->db->query("SELECT * FROM tbl_dosen WHERE nidn ='$kd'");
                $hsl = $cek->num_rows();

                if($hsl>0){
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-danger fade in'> 
                        	<a href='#' class='close' data-dismiss='alert'>&times;</a>                           
                            <strong> Simpan data gagal (data sudah ada !!) </strong>
                        </div> 
                        ");

                    header('location:'.base_url().'Dosen');
                }else{
                    $data = array(                         
                                   
                        'nidn' => $this->input->post('NmNidn'),                         
                        'nama_dosen' => $this->input->post('NmNama'),                        
                        'jk_dosen' => $this->input->post('NmJk'),                                                
                        'alamat_dosen' => $this->input->post('NmAlmt'), 
                        'hp_dosen' => $this->input->post('NmHp')
                         );

                    $dk = $this->Mymodel->Insert('tbl_dosen', $data); //function model 
                    //var_dump($data2);
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-success fade in'>
                            <a href='#' class='close' data-dismiss='alert'>&times;</a>
                            <strong>Data Berhasil Disimpan</strong>
                        </div>");

                    header('location:'.base_url().'Dosen');
                }

	        }elseif(isset($_POST['BtnEdit'])){
	            $kd = $this->input->post('NmNidn');
	            $nama = $_POST['NmNama'];
	            $jk = $_POST['NmJk'];                
                $alm = $_POST['NmAlmt'];
                $hp = $_POST['NmHp'];	
                
             //    $tgl = date('Y-m-d', strtotime($_POST['NmTgl']));
	            // $kdkr = $_POST['NmPj']; 
                
	            
	            $data = array( 'nama_dosen' => $nama,'jk_dosen' => $jk,'alamat_dosen'=>$alm,'hp_dosen' => $hp);
	            $where = array('nidn' => $kd);
	            $this->load->model('Mymodel');
	            $res = $this->Mymodel->Update('tbl_dosen', $data, $where);


	            $this->session->set_flashdata("msg","
	            <div class='alert alert-success alert-block fade in'> 
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>                         
	                <strong> Data Berhasil Dirubah (Edit data sucess !!) </strong>
	            </div> 
	            ");

	            header('location:'.base_url().'Dosen');
	        }else{
	            echo "error";
	        }
	}

	public function hapus($kd){
        $kd = array('nidn' => $kd);
        $this->load->model('Mymodel');
        $this->Mymodel->Delete('tbl_dosen', $kd);
        header('location:'.base_url().'Dosen');    

    }
}
