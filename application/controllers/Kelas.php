<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {
    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if($this->session->userdata('id')==''){
            redirect('Web');
        }
        
    }

	public function index()
	{
        var_dump($this->session->userdata('logged_in')) ;  
		$this->load->model('Mymodel');
        $dt_kls = $this->Mymodel->GetTabel('tbl_kelas');        
        
        $title = 'Kelas';
        $data = array('dtkls' => $dt_kls,'judul'=> $title); //judul untuk dipanggil ke view
        //$this->load->view('header');
        $this->load->view('Adm/v_kelas',$data);
        
	}

	public function simpan(){
		$this->load->model('Mymodel');
       
        if(isset($_POST['BtnSimpan'])){

                $kd = $this->input->post("txt_kode");
                $cek = $this->db->query("SELECT * FROM tbl_kelas WHERE kd_kelas ='$kd'");
                $hsl = $cek->num_rows();

                if($hsl>0){
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-danger fade in'> 
                        	<a href='#' class='close' data-dismiss='alert'>&times;</a>                           
                            <strong> Simpan data gagal (data sudah ada !!) </strong>
                        </div> 
                        ");

                    header('location:'.base_url().'Kelas');
                }else{
                    $data = array(                         
                                   
                        'kd_kelas' => $this->input->post('txt_kode'),                         
                        'nama_kelas' => $this->input->post('txt_nama'),
                        'kapasitas' => $this->input->post('txt_kapasitas')
                         );

                    $dk = $this->Mymodel->Insert('tbl_kelas', $data); //function model 
                    //var_dump($data2);
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-success fade in'>
                            <a href='#' class='close' data-dismiss='alert'>&times;</a>
                            <strong>Data Berhasil Disimpan</strong>
                        </div>");

                    header('location:'.base_url().'Kelas');
                }

	        }elseif(isset($_POST['BtnEdit'])){
	            $kd = $this->input->post('txt_kode');
	            $nama = $_POST['txt_nama'];
	            $kap = $_POST['txt_kapasitas'];
	            
	            $data = array( 'nama_Kelas' => $nama,'kapasitas' => $kap);
	            $where = array('kd_kelas' => $kd);
	            $this->load->model('Mymodel');
	            $res = $this->Mymodel->Update('tbl_kelas', $data, $where);


	            $this->session->set_flashdata("msg","
	            <div class='alert alert-success alert-block fade in'> 
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>                         
	                <strong> Data Berhasil Dirubah (Edit data sucess !!) </strong>
	            </div> 
	            ");

	            header('location:'.base_url().'Kelas');
	        }else{
	            echo "error";
	        }
	}

	public function hapus($kd){
        $kd = array('kd_kelas' => $kd);
        $this->load->model('Mymodel');
        $this->Mymodel->Delete('tbl_Kelas', $kd);
        header('location:'.base_url().'Kelas');    

    }
}
