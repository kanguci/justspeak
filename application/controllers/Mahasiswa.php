<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {
    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if($this->session->userdata('id')==''){
            redirect('Web');
        }
        
    }

	public function index()
	{
        var_dump($this->session->userdata('logged_in')) ;  
		$this->load->model('Mymodel');
        $dt_mhs = $this->Mymodel->GetTabel('tbl_mhs');        
        
        $title = 'Mahasiswa';
        $data = array('dtmhs' => $dt_mhs,'judul'=> $title); //judul untuk dipanggil ke view
        //$this->load->view('header');
        $this->load->view('Adm/v_mhs',$data);
        //$this->load->view('side_menu');        
        //$this->load->view('footer');
	}

	public function simpan(){
		$this->load->model('Mymodel');
       
        if(isset($_POST['BtnSimpan'])){

                $kd = $this->input->post("NmKd");
                $cek = $this->db->query("SELECT * FROM tbl_Mahasiswa WHERE kd_Mahasiswa ='$kd'");
                $hsl = $cek->num_rows();

                if($hsl>0){
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-danger fade in'> 
                        	<a href='#' class='close' data-dismiss='alert'>&times;</a>                           
                            <strong> Simpan data gagal (data sudah ada !!) </strong>
                        </div> 
                        ");

                    header('location:'.base_url().'Mahasiswa');
                }else{
                    $data_Mahasiswa = array(                         
                                   
                        'kd_Mahasiswa' => $this->input->post('NmKd'),  
                        'Mahasiswa' => $this->input->post('NmDev')
                         );

                    $dk = $this->Mymodel->Insert('tbl_Mahasiswa', $data_Mahasiswa); //function model 
                    //var_dump($data2);
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-success fade in'>
                            <a href='#' class='close' data-dismiss='alert'>&times;</a>
                            <strong>Data Berhasil Disimpan</strong>
                        </div>");

                    header('location:'.base_url().'Mahasiswa');
                }

	        }elseif(isset($_POST['BtnEdit'])){
	            $kd = $this->input->post('NmKd');
	            $dev = $_POST['NmDev'];
	            $data = array( 'Mahasiswa' => $dev);
	            $where = array('kd_Mahasiswa' => $kd);
	            $this->load->model('Mymodel');
	            $res = $this->Mymodel->Update('tbl_Mahasiswa', $data, $where);


	            $this->session->set_flashdata("msg","
	            <div class='alert alert-success alert-block fade in'> 
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>                         
	                <strong> Data Berhasil Dirubah (Edit data sucess !!) </strong>
	            </div> 
	            ");

	            header('location:'.base_url().'Mahasiswa');
	        }else{
	            echo "error";
	        }
	}

	public function hapus($kd){
        $kd = array('kd_Mahasiswa' => $kd);
        $this->load->model('Mymodel');
        $this->Mymodel->Delete('tbl_Mahasiswa', $kd);
        header('location:'.base_url().'Mahasiswa');    

    }
}
