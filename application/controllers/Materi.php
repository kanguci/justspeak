<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends CI_Controller {
    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if($this->session->userdata('id')==''){
            redirect('Web');
        }
        
    }

	public function index()
	{
        var_dump($this->session->userdata('logged_in')) ;  
		$this->load->model('Mymodel');
        $dt_mk = $this->Mymodel->GetTabel('tbl_mk');        
        
        $title = 'Materi';
        $data = array('dtmk' => $dt_mk,'judul'=> $title); //judul untuk dipanggil ke view
        //$this->load->view('header');
        $this->load->view('Adm/v_materi',$data);
        
	}

	public function simpan(){
		$this->load->model('Mymodel');
       
        if(isset($_POST['BtnSimpan'])){

                $kd = $this->input->post("txt_kode");
                $cek = $this->db->query("SELECT * FROM tbl_mk WHERE kd_mk ='$kd'");
                $hsl = $cek->num_rows();

                if($hsl>0){
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-danger fade in'> 
                        	<a href='#' class='close' data-dismiss='alert'>&times;</a>                           
                            <strong> Simpan data gagal (data sudah ada !!) </strong>
                        </div> 
                        ");

                    header('location:'.base_url().'Materi');
                }else{
                    $data = array(                         
                                   
                        'kd_mk' => $this->input->post('txt_kode'),                         
                        'nama_mk' => $this->input->post('txt_nama')                        
                         );

                    $dk = $this->Mymodel->Insert('tbl_mk', $data); //function model 
                    //var_dump($data2);
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-success fade in'>
                            <a href='#' class='close' data-dismiss='alert'>&times;</a>
                            <strong>Data Berhasil Disimpan</strong>
                        </div>");

                    header('location:'.base_url().'Materi');
                }

	        }elseif(isset($_POST['BtnEdit'])){
	            $kd = $this->input->post('txt_kode');
	            $nama = $_POST['txt_nama'];
	            // $kap = $_POST['txt_kapasitas'];
	            
	            $data = array( 'nama_mk' => $nama);
	            $where = array('kd_mk' => $kd);
	            $this->load->model('Mymodel');
	            $res = $this->Mymodel->Update('tbl_mk', $data, $where);


	            $this->session->set_flashdata("msg","
	            <div class='alert alert-success alert-block fade in'> 
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>                         
	                <strong> Data Berhasil Dirubah (Edit data sucess !!) </strong>
	            </div> 
	            ");

	            header('location:'.base_url().'Materi');
	        }else{
	            echo "error";
	        }
	}

	public function hapus($kd){
        $kd = array('kd_Materi' => $kd);
        $this->load->model('Mymodel');
        $this->Mymodel->Delete('tbl_Materi', $kd);
        header('location:'.base_url().'Materi');    

    }
}
