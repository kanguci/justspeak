<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if ($this->session->userdata('id') == '') {
            redirect('Web');
        }

    }

    public function index()
    {
        var_dump($this->session->userdata('logged_in'));
        $this->load->model('Mymodel');
        $dt_register = $this->Mymodel->GetTabel('tbl_registrasi');
        $dt_peserta = $this->Mymodel->GetTabel('vw_peserta');

        $title = 'Peserta';
        $data = array('dtpeserta' => $dt_peserta, 'dtreg' => $dt_register, 'judul' => $title); //judul untuk dipanggil ke view
        //$this->load->view('header');
        $this->load->view('Adm/v_peserta', $data);
        //$this->load->view('side_menu');        
        //$this->load->view('footer');
    }

    public function simpan()
    {
        $this->load->model('Mymodel');

        if (isset($_POST['BtnSimpan'])) {

            $No = $this->input->post("txt_nomor");
            $tgl = $this->input->post('txt_tgl');
            $bln = $this->input->post('txt_bln');
            $thn = $this->input->post('txt_thn');
            $ttl = $tgl . " " . $bln . " " . $thn;

            // proses format input tanggal lahir (d/m/Y) ke mysql format (Y-m-d)
            $dob = $this->input->post('date_of_birth');
            $ttl = $dob ? date('Y-m-d', strtotime(str_replace('/', '-', $dob))) : null;


            $cek = $this->db->query("SELECT * FROM tbl_peserta WHERE no_peserta ='$No'");
            $hsl = $cek->num_rows();

            if ($hsl > 0) {
                $this->session->set_flashdata("msg", "
                        <div class='alert alert-danger fade in'> 
                        	<a href='#' class='close' data-dismiss='alert'>&times;</a>                           
                            <strong> Simpan data gagal (data sudah ada !!) </strong>
                        </div> 
                        ");

                header('location:' . base_url() . 'Peserta');
            } else {
                $data_Peserta = array(

                    'no_peserta' => $this->input->post('txt_nomor'),
                    'nama_peserta' => $this->input->post('txt_nama'),
                    'tempat_lahir' => $this->input->post('txt_tempat'),
                    'ttl' => $ttl,
                    'jk_peserta' => $this->input->post('txt_jk'),
                    'alamat_peserta' => $this->input->post('txt_alamat'),
                    'no_hp' => $this->input->post('txt_hp'),
                    'no_reg' => $this->input->post('txt_noreg'),
                    'biaya_reg' => $this->input->post('txt_biaya')
                );

                $dk = $this->Mymodel->Insert('tbl_peserta', $data_Peserta); //function model
                //var_dump($data2);
                $this->session->set_flashdata("msg", "
                        <div class='alert alert-success fade in'>
                            <a href='#' class='close' data-dismiss='alert'>&times;</a>
                            <strong>Data Berhasil Disimpan</strong>
                        </div>");

                header('location:' . base_url() . 'Peserta');
            }

        } elseif (isset($_POST['BtnEdit'])) {
            $tgl = $this->input->post('txt_tgl');
            $bln = $this->input->post('txt_bln');
            $thn = $this->input->post('txt_thn');
            $ttl = $tgl . " " . $bln . " " . $thn;

            // proses format input tanggal lahir (d/m/Y) ke mysql format (Y-m-d)
            $dob = $this->input->post('date_of_birth');
            $ttl = $dob ? date('Y-m-d', strtotime(str_replace('/', '-', $dob))) : null;

            $No = $this->input->post('txt_nomor');
            $Nama = $_POST['txt_nama'];
            $Tempat = $this->input->post('txt_tempat');
            $JK = $_POST['txt_jk'];
            $Alamat = $this->input->post('txt_alamat');
            $Hp = $_POST['txt_hp'];

            $Biaya = $this->input->post('txt_biaya');
            // $Nama = $_POST['txt_nama'];

            $data = array('nama_peserta' => $Nama, 'tempat_lahir' => $Tempat, 'ttl' => $ttl, 'jk_peserta' => $JK, 'alamat_peserta' => $Alamat, 'no_hp' => $Hp, 'biaya_reg' => $Biaya);
            $where = array('no_peserta' => $No);
            $this->load->model('Mymodel');
//            dd($data, $where);
            $res = $this->Mymodel->Update('tbl_peserta', $data, $where);


            $this->session->set_flashdata("msg", "
	            <div class='alert alert-success alert-block fade in'> 
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>                         
	                <strong> Data Berhasil Dirubah (Edit data sucess !!) </strong>
	            </div> 
	            ");

            header('location:' . base_url() . 'Peserta');
        } else {
            echo "error";
        }
    }

    public function hapus($kd)
    {
        $kd = array('no_peserta' => $kd);
        $this->load->model('Mymodel');
        $this->Mymodel->Delete('tbl_peserta', $kd);
        header('location:' . base_url() . 'Peserta');

    }
}
