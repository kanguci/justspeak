<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {
    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        // if($this->session->userdata('id')==''){
        //     redirect('Web');
        // }        
    }

	public function index()
	{
        // var_dump($this->session->userdata('logged_in')) ;  
		// $this->load->model('Mymodel');
  //       $dt_reg = $this->Mymodel->GetTabel('tbl_registrasi');        
        
        $title = 'Registrasi';
        $data = array('judul'=> $title); //judul untuk dipanggil ke view
        //$this->load->view('header');
        $this->load->view('vw_registrasi',$data);
        //$this->load->view('side_menu');        
        //$this->load->view('footer');
	}

    public function generate(){
        $tglDef = date('Y-m-d');
        $nama = $this->input->post('txt_nama'); //date('Y-m-d', strtotime($this->input->post('nm_tgl'))),
        $tempat = $this->input->post('txt_tempat');
        $tgl = $this->input->post('txt_tgl');
        $bln = $this->input->post('txt_bln');
        $thn = $this->input->post('txt_thn');
        $ttl = $tgl." ".$bln." ".$thn;
        $jk = $this->input->post('txt_jk');
        $alamat = $this->input->post('txt_alamat');
        $hp = $this->input->post('txt_hp');


        // pilih nomor daftar
        // $cekkode = $this->db->query("SELECT no_reg FROM tbl_registrasi ORDER BY no_reg DESC LIMIT 1");
        // $jumlah = $cekkode->num_rows();
        // foreach ($cekkode->result() as $ck) {
        //     # code...
        //     $No = $ck->no_reg;
        // }

        // if($jumlah <> 0){
        //    $Kode = intval($No)+1;           
        // }else{
        //     $Kode = 1;
        // }

        // $KodeMax =str_pad($Kode, 10, "0", STR_PAD_LEFT);
        // $No_reg = 'DF-'.$KodeMax;
        // $this->db->query("INSERT INTO tbl_registrasi VALUES ('$KodeMax','$tglDef','$nama','$ttl','$jk','$alamat','$hp')");


        //kode otomatis
          $this->db->select('RIGHT(tbl_registrasi.no_reg, 7) as kode', FALSE);
          $this->db->order_by('no_reg','DESC');    
          $this->db->limit(1);    
          $query = $this->db->get('tbl_registrasi');      //cek dulu apakah ada sudah ada kode di tabel.    
          if($query->num_rows() <> 0){      
           //jika kode ternyata sudah ada.      
           $data = $query->row();      
           $kode = intval($data->kode) + 1;    
          }else{      
           //jika kode belum ada      
           $kode = 1;    
          }
          $kodemax = str_pad($kode, 7, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
          $kodejadi = "DF-".$kodemax;    // hasilnya ODJ-9921-0001 dst.

          $this->db->query("INSERT INTO tbl_registrasi VALUES ('$kodejadi','$tglDef','$nama','$tempat','$ttl','$jk','$alamat','$hp')");
          $this->session->set_flashdata("msg","
            <div class='alert alert-success fade in'>
                <a href='#' class='close' data-dismiss='alert'>&times;</a>
                <strong>Terima Kasih, Dan selamat datang</strong>
            </div>");
          redirect('Web');

          return $kodejadi;

          }
            
    // }


	public function simpan(){
		$this->load->model('Mymodel');
       
        if(isset($_POST['BtnSimpan'])){

                $No = $this->input->post("NmKd");
                $cek = $this->db->query("SELECT * FROM tbl_registrasi WHERE no_reg ='$No'");
                $hsl = $cek->num_rows();

                if($hsl>0){
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-danger fade in'> 
                        	<a href='#' class='close' data-dismiss='alert'>&times;</a>                           
                            <strong> Simpan data gagal (data sudah ada !!) </strong>
                        </div> 
                        ");

                    header('location:'.base_url().'Mahasiswa');
                }else{
                    $data_Mahasiswa = array(                         
                                   
                        'kd_Mahasiswa' => $this->input->post('NmKd'),  
                        'Mahasiswa' => $this->input->post('NmDev')
                         );

                    $dk = $this->Mymodel->Insert('tbl_Mahasiswa', $data_Mahasiswa); //function model 
                    //var_dump($data2);
                    $this->session->set_flashdata("msg","
                        <div class='alert alert-success fade in'>
                            <a href='#' class='close' data-dismiss='alert'>&times;</a>
                            <strong>Data Berhasil Disimpan</strong>
                        </div>");

                    header('location:'.base_url().'Mahasiswa');
                }

	        }elseif(isset($_POST['BtnEdit'])){
	            $kd = $this->input->post('NmKd');
	            $dev = $_POST['NmDev'];
	            $data = array( 'Mahasiswa' => $dev);
	            $where = array('kd_Mahasiswa' => $kd);
	            $this->load->model('Mymodel');
	            $res = $this->Mymodel->Update('tbl_Mahasiswa', $data, $where);


	            $this->session->set_flashdata("msg","
	            <div class='alert alert-success alert-block fade in'> 
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>                         
	                <strong> Data Berhasil Dirubah (Edit data sucess !!) </strong>
	            </div> 
	            ");

	            header('location:'.base_url().'Mahasiswa');
	        }else{
	            echo "error";
	        }
	}

	public function hapus($kd){
        $kd = array('kd_Mahasiswa' => $kd);
        $this->load->model('Mymodel');
        $this->Mymodel->Delete('tbl_Mahasiswa', $kd);
        header('location:'.base_url().'Mahasiswa');    

    }
}
