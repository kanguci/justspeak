<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soal_test extends CI_Controller {
    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if($this->session->userdata('id')==''){
            redirect('Web');
        }
        
    }

	public function index()
	{
		$this->load->model('Mymodel');
        $soal = $this->Mymodel->GetTabel('tbl_soal_tes');         
        $title = 'Soal test';
        $data = array('dtsoal' => $soal,'judul'=> $title);
        $this->load->view('Adm/v_soal_test',$data);
        //$this->load->view('side_menu');        
        //$this->load->view('footer');
	}

    public function form_tambah(){

        $this->load->model('Mymodel');
        // $dtkar = $this->db->select('*')->from('tbl_karyawan')->where('bagian','Driver')->get();
        $tbl_soal = $this->Mymodel->GetTabel('tbl_soal_tes');
        // $tbl_barang = $this->Mymodel->GetTabel('tbl_barang');
        // $tbl_toko = $this->Mymodel->GetTabel('tbl_toko');
        $title = 'Tambah Soal';

        $data = array('dtsoal' => $tbl_soal,'judul' => $title);
        $this->load->view('Adm/v_add_soal_test',$data);
        
    }

	public function simpan(){
		$this->load->model('Mymodel');
        $this->db->select('RIGHT(tbl_soal_tes.no_soal, 3) as kode', FALSE);
          $this->db->order_by('no_soal','DESC');    
          $this->db->limit(1);    
          $query = $this->db->get('tbl_soal_tes');      
          if($query->num_rows() <> 0){      
           
           $data = $query->row();      
           $kode = intval($data->kode) + 1;    
          }else{      
           //jika kode belum ada      
           $kode = 1;    
          }

          $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT); 
          $kodejadi = $kodemax;
       
        $data = array(                         
                //'tgl_peng' => date('Y-m-d', strtotime($this->input->post('NmTgl'))),          
                'no_soal' => $kodejadi,
                'soal' => $this->input->post('txt_soal'),
                'opt_a' => $this->input->post('txt_a'),
                'opt_b' => $this->input->post('txt_b'),
                'opt_c' => $this->input->post('txt_c'),
                'opt_d' => $this->input->post('txt_d'),
                'kunci' => $this->input->post('txt_kunci')
                 );

            $dk = $this->Mymodel->Insert('tbl_soal_tes', $data);    
            //          
            header('location:'.base_url().'Soal_test');
            return $kodejadi;
        }

        

    public function form_edit($kd){
        $this->load->model('Mymodel');
        $title = 'Edit Pre Order';
        $tbl_barang = $this->Mymodel->GetTabel('tbl_barang');
        // $tbl_toko = $this->Mymodel->GetTabel('tbl_toko');
        $dtpo = $this->db->query("SELECT * FROM vw_po WHERE kd_soal ='$kd' ");

        foreach($dtpo->result() as $j){
            $kdpo = $j->kd_soal;
            $tgl = $j->tgl_po;            
            $kdbarang = $j->kd_barang;           
            $nmbarang= $j->nama_barang;
            $merk= $j->merk;
            $jml = $j->jml_po;
            $ket = $j->ket;
        }

        $data = array(
                'Brg' => $tbl_barang, 'judul'=>$title,
                'KdPo'=>$kdpo,
                'Tgl' => $tgl,                
                'KdBr'=>$kdbarang,                
                'NmBr'=>$nmbarang, 
                'Mrk'=>$merk,               
                'Jml'=>$jml,
                'Ket'=>$ket
                 );          

        $this->load->view('Usr/v_frmeditorder',$data);

    }
	
    public function edit(){
        $kd_soal = $this->input->post('NmKdPo');
        $tgl = date('Y-m-d', strtotime($this->input->post('nm_tgl')));
        $kd_karyawan = $this->input->post('NmKdKar');
        $kd_barang =$this->input->post('NmKdbr');
        $jml = $this->input->post('NmJml');
        $ket = $this->input->post('NmKet');
        // $status = $this->input->post('NmSt');

        $data = array
            (   
                'tgl_po' => $tgl, 
                'kd_karyawan' => $kd_karyawan,                
                'kd_barang' => $kd_barang,
                'jml_po' => $jml,
                // 'kd_toko' => $kd_toko,                
                'ket' => $ket
            );
        $where = array
            (
                'kd_soal' => $kd_soal,
            );

        $this->load->model('Mymodel');
        $res = $this->Mymodel->Update('tbl_soal_tes', $data, $where);

        if ($res>0) {
        redirect('Po','refresh');
        } 

    }

	public function hapus($kd){
        $kd = array('kd_soal' => $kd);
        $this->load->model('Mymodel');
        $this->Mymodel->Delete('tbl_soal_tes', $kd);
        header('location:'.base_url().'Po');    

    }
}
