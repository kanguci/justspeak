<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if($this->session->userdata('id')==''){
            redirect('Web');
        }
        
    }

	public function index()
	{
//        var_dump($this->session->userdata('logged_in')) ;  // jangn pake "var_dump" lg, gunain function "dd" udah wa buatin
		$this->load->model('Mymodel');        
        $dtsoal = $this->Mymodel->GetTabel('tbl_soal_tes');  
        $Jml=$this->db->get('tbl_soal_tes')->num_rows();
        $judul = 'Test';

        /** @var CI_DB_query_builder $db */
        /** @var CI_DB_result $query */
        /** @var CI_Input $input */
        $db = $this->db; // ini shortcut instant dari $this->db (biar kebaca semua method kalo pake phpstorm)
        $input = $this->input; // ini shortcut instant dari $this->input (biar kebaca semua method kalo pake phpstorm)
        $query = $db->from('tbl_tes')->where("no_ujian IS NOT NULL")->where('no_ujian', $input->get('id'))->get('', 1);
//        dd($db->last_query(), $query->result());
        $dttes = $query->row();
        $query = $db->from('tbl_hasil_tes')
            ->where("no_ujian IS NOT NULL")
            ->where('no_ujian', $input->get('id'))
            ->get();
        $answers = [];
        foreach ($query->result() as $item)
            $answers[$item->no_soal] = $item->jb_tes;
//        dd($answers); // kalo mau debug isi variable "answers"

        // $data = array('dtsoal' => $dtsoal,'judul'=> $title, 'No'=>$no,'Jb'=>$jb); //judul untuk dipanggil ke view
//        $data = array('dtsoal' => $dtsoal,'Jml'=>$jumlah,'judul'=> $title);
        // $this->load->view('blank_page');
//        $this->load->view('v_test',$data);
        $this->load->view('v_test',get_defined_vars()); // gunain function "get_defined_vars" otomatis semua variable yang udah d define d compact sebagai array jadi gk perlu repot (norak!)

	}

	public function simpan(){
        $this->load->helper('array');
        $this->load->model('Mymodel');
        /** @var CI_DB_query_builder $db */
        $db = $this->db; // ini shortcut instant dari $this->db (biar kebaca semua method kalo phpstorm)
        $jml=$this->db->get('tbl_soal_tes')->num_rows();
        $nomor = $this->input->post("txt_nomor");

        $db->trans_start(); // biasain gunain db transaction sebelum query ke database (kalo ada salah input atau hapus, gk langsung di jalanin queryny)

        // insert data ujian/test terlebih dahulu
        $preUjian = [
            'kd_soal' => random_element(range('A', 'D')), // hanya contoh (random) ganti kalo udh ada ketentuan kode soalny
            'no_peserta' => $this->session->userdata('id'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
//        dd($preUjian); // kalo mau debug isi dari variable "preUjian"
        $db->insert('tbl_tes', $preUjian);
        $noUjian = $db->insert_id();

        if($answers = (array)$this->input->post('jawab')){
            // dd($answers);
            $preInsert = []; // prepare semua jawaban dari inputan (radio button)
            /** @var CI_DB_result $query */
            $query = $db->from('tbl_soal_tes')->get();
            $dtKunci = []; // get all kunci jawaban untuk d kompare k jawaban untuk menentukan benar atau salah
            foreach ($query->result() as $item)
                $dtKunci[intval($item->no_soal)] = $item->kunci;
            $countBenar  = 0; // hitung jawaban benar
            $countSalah  = 0; // hitung jawaban salah
            foreach($answers as $noSoal => $answer){
                $countBenar += strcasecmp($answer, $dtKunci[$noSoal]) == 0 ? 1 : 0;
                $countSalah += strcasecmp($answer, $dtKunci[$noSoal]) != 0 ? 1 : 0;
                $preInsert[] = [
                    'jb_tes' => $answer,
                    'no_soal' => $noSoal,
                    // udah gk perlu nyimpen nomor peserta d table "tbl_hasil_tes" karna udah ada d table "tbl_tes"
//                    'no_peserta' => $this->session->userdata('id'), // pastikan nomor peserta d registrasi waktu login
                    'no_ujian' => $noUjian
                ];
            }

            // update jumlah benar/salah & nilai di table ujian/tes
            $db->update('tbl_tes', [
                'jb_benar' => $countBenar,
                'jb_salah' => $countSalah,
                'nilai' => $countBenar / $query->num_rows() * 100 // ganti rumus ini kalo udah ada ketentuanny
            ], "no_ujian = $noUjian");

//            dd($db->from('tbl_tes')->where('no_ujian', $noUjian)->get()->row());

            // dd($preInsert); // kalo mau debug isi dari variable "preInsert"
            // insert hasil test gunain batch insert biar code & query lebih efisien
            $db->insert_batch('tbl_hasil_tes', $preInsert); // insert batch gunanya untuk run multiple insert dalam 1 query

            if ($db->trans_status() === FALSE){ // kalo ada error d query, maka semua query akan dbatalkan, trus nampil log errorny
                log_message();
            }
            
        }
        $this->db->trans_complete(); // run db transaction complete untuk menyimpan semua query (kalo gk dpanggil semua query gk akan ngefek ke database)
        redirect('/test', 'refresh');

        // if(isset($_POST) && !empty($_POST)){

        // // $Jb=$this->input->post('jawab1');
        // // $NoSoal=$this->input->post('txt_nomor');

        // //     $post = $this->input->post();
        // //     foreach ($post['jawab'] as $key => $value) {
        // //     //masukkan data ke variabel array jika kedua form tidak kosong
        // //     if ($post['jawab'][$key] != '' || $post['nomor'][$key] != '')
        // //     {
        // //        $simpan[] = array(
        // //           'jb_tes' => $post['jawab'][$key],
        // //           'no_soal'=> $post['nomor'][$key]
        // //        );
        // //     }
        // //  }

        // // $this->app->multi_insert('tbl_hasil_tes', $simpan);
        
        // // $data = array(       
        // //     array(                 
                                   
        // //     'jb_tes' => $this->input->post('jawab1'),                         
        // //     'no_soal' => $this->input->post('txt_nomor')
        // //     )
        // //      );

        // // $item = $this->Mymodel->insert_batch('tbl_hasil_tes', $data);

 
        // for($i = 1; $i <= $jml; $i++)
        // {
        //     //echo $i . '<br />';
        //     $data = array(         load
        //     'jb_tes' => 'jawab'.$i,                         
        //     'no_soal' => $nomor
        //      );
        //     $item = $this->Mymodel->insert('tbl_hasil_tes', $data);
        // }
 
        // $item = $this->Mymodel->insert('tbl_hasil_tes', $data);
        

        // }else{
        //     echo "error";
        // }
        
	}

	public function hapus($kd){
        $kd = array('kd_Mahasiswa' => $kd);
        $this->load->model('Mymodel');
        $this->Mymodel->Delete('tbl_Mahasiswa', $kd);
        header('location:'.base_url().'Mahasiswa');    

    }

    public function hasil()
    {
        $this->load->model('Mymodel');
        $judul = 'Data Hasil Test';
        /** @var CI_DB_query_builder $db */
        $db = $this->db;
        $run = $db
            ->select([
                'a.*',
                'b.nama_peserta'
            ])
            ->from('tbl_tes a')
            ->join('tbl_peserta AS b', 'a.no_peserta = b.no_peserta', 'left')
            ->get();
//        dd($testResults->result());
        $testResults = $run->result();
        $this->load->view('Adm/v_hasil_test', get_defined_vars());
    }

    public function update($id)
    {
        /** @var CI_DB_query_builder $db */
        $db = $this->db;
        /** @var CI_Input $input */
        $input = $this->input;
        $sql = $db
            ->from('tbl_tes a')
            ->join('tbl_peserta AS b', 'a.no_peserta = b.no_peserta', 'left')
            ->where('a.no_ujian', $id)
            ->get();
        $test = $sql->row();

        if(!$test){
            throw new Exception('Data "Test" tidak ditemukan!'); // jangan proses apapun kalo data test salah or gk ada
            return false;
        }

//        dd($input->post(), $id, $sql->row());

        $db->trans_start(); // biasain gunain db transaction sebelum query ke database (kalo ada salah input atau hapus, gk langsung di jalanin queryny)

        if($answers = (array)$input->post('jawab')){
            // dd($answers);
            $preInsert = [];
            /** @var CI_DB_result $query */
            $query = $db->from('tbl_soal_tes')->get();
            $dtKunci = [];
            foreach ($query->result() as $item)
                $dtKunci[intval($item->no_soal)] = $item->kunci;
            $countBenar  = 0;
            $countSalah  = 0;
            foreach($answers as $noSoal => $answer){
                $countBenar += strcasecmp($answer, $dtKunci[$noSoal]) == 0 ? 1 : 0;
                $countSalah += strcasecmp($answer, $dtKunci[$noSoal]) != 0 ? 1 : 0;
                $preInsert[] = [
                    'jb_tes' => $answer,
                    'no_soal' => $noSoal,
                    'no_ujian' => $test->no_ujian
                ];
            }

            $db->update('tbl_tes', [
                'jb_benar' => $countBenar,
                'jb_salah' => $countSalah,
                'nilai' => $countBenar / $query->num_rows() * 100,
                'updated_at' => date('Y-m-d H:i:s'), // column ini untuk menandai kapan terakhir kali tes di-update
            ], "no_ujian = $test->no_ujian");

//            dd($db->from('tbl_tes')->where('no_ujian', $test->no_ujian)->get()->row());

//             dd($preInsert); // kalo mau debug isi dari variable "preInsert"

            $db->delete('tbl_hasil_tes', ['no_ujian' => $test->no_ujian]); // sebelum insert jawaban, hapus dulu jawaban test sebelumny

//            dd($db->from('tbl_hasil_tes')->where('no_ujian', $test->no_ujian)->get()->result()); // untuk ngecek jawaban sebelumny udh k hapus

            $db->insert_batch('tbl_hasil_tes', $preInsert);

//            dd($db->from('tbl_hasil_tes')->where('no_ujian', $test->no_ujian)->get()->result()); // kalo mau lihat jawaban yg sudah ter-update

            if ($db->trans_status() === FALSE){
                log_message();
            }

        }
        $db->trans_complete(); // run db transaction complete untuk menyimpan semua query (kalo gk dpanggil semua query gk akan ngefek ke database)
        redirect("/Test?id=$test->no_ujian", 'refresh');
    }
}
