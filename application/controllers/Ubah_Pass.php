<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubah_Pass extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        if($this->session->userdata('id')==''){
            redirect('Web');
        }
        
    }
    
	public function index()
	{
		// $this->load->model('Mymodel');
        $this->load->view('header');
        $this->load->view('Adm/v_ubah_pass');
        $this->load->view('side_menu');
	}

	public function edit(){
        //$tgl_kirim = $this->input->post('NmTgl');   
             
        $id = $this->input->post('NmKd');
        $user = $this->input->post('Nmuser');
        $pswd = $this->input->post('NmPswd');

        $this->db->query("UPDATE tbl_user SET password='$pswd' WHERE id ='$id'");

        $this->session->set_flashdata("msg","
                <div class='alert alert-success fade in'>
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>
                    <strong>Password Berhasil Diubah</strong>
                </div>");       
            
        redirect('Ubah_Pass','refresh');      
       
    }

    public function form_user()
    {
        // $this->load->model('Mymodel');
        $this->load->view('header');
        $this->load->view('Usr/v_ubah_pass');
        $this->load->view('Usr/side_menu');
    }

    public function edit_pass_user(){
        //$tgl_kirim = $this->input->post('NmTgl');   
             
        $id = $this->input->post('NmKd');
        $user = $this->input->post('Nmuser');
        $pswd = $this->input->post('NmPswd');

        $this->db->query("UPDATE tbl_user SET password='$pswd' WHERE id ='$id'");

        $this->session->set_flashdata("msg","
                <div class='alert alert-success fade in'>
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>
                    <strong>Password Berhasil Diubah</strong>
                </div>");       
            
        redirect('Ubah_Pass/form_user','refresh');      
       
    }

	
}
