<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_file extends CI_Controller {
 public function __construct()
    {
            parent::__construct();
            $this->load->helper(array('form', 'url'));
    }

    function add(){
        if($this->input->post('userSubmit')){
            $this->load->model('Mymodel');
            //Check whether user upload picture
            if(!empty($_FILES['picture']['name'])){
                $config['upload_path'] = 'uploads/images/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['picture']['name'];
                
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('picture')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                }else{
                    $picture = '';
                }
            }else{
                $picture = '';
            }
            
            //Prepare array of user data
            $data = array(
                'title' => $this->input->post('name'),
                'file_name' => $this->input->post('email'),
                'picture' => $picture
            );
            
            // Pass user data to model
            $insertUserData = $this->user->insert($data);
            
            
            // Storing insertion status message.
            if($insertUserData){
                $this->session->set_flashdata('success_msg', 'User data have been added successfully.');
            }else{
                $this->session->set_flashdata('error_msg', 'Some problems occured, please try again.');
            }


            // $gambar = $this->Mymodel->Insert('tbl_gambar', $data);
            // if($gambar){
            //     $this->session->set_flashdata('success_msg', 'User data have been added successfully.');
            // }else{
            //     $this->session->set_flashdata('error_msg', 'Some problems occured, please try again.');
            // }
        }
        //Form for adding user data
        $this->load->view('v_upload');
    }

        


}
