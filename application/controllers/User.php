<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct(){
        parent::__construct();
        // if($this->session->userdata('logged_in')<>'yes'){
        //     redirect('Web');
        // }

        // if($this->session->userdata('id')==''){
        //     redirect('Web');
        // }
        
    }

	public function index()
	{
		if($this->session->userdata('status')=='user'){
            $bc = array(
                'id'     => $this->session->userdata('id'),
                'status' => $this->session->userdata('status'),
                'username' => $this->session->userdata('username'),
            );
            $this->load->view('header',$bc);              
            $this->load->view('home'); 
            $this->load->view('Usr/side_menu');        
            $this->load->view('footer');
        }

        
	}

    public function tampil_user(){
       var_dump($this->session->userdata('logged_in')) ;  
        $this->load->model('Mymodel');
        //$dt_user = $this->Mymodel->GetTabel('tbl_user');        
        $dt_user = $this->db->query("SELECT * FROM tbl_user where id <> '1' ");
        $title = 'Data User';
        $data = array('dtuser' => $dt_user,'judul'=> $title); //judul untuk dipanggil ke view
        //$this->load->view('header');
        $this->load->view('Adm/v_user',$data);
    }

	
    public function simpan(){
        if(isset($_POST['BtnEdit'])){
                $this->load->model('Mymodel');
                $kd = $this->input->post('NmKode');
                $nama = $this->input->post('NmNama');                
                $sts = $_POST['NmSts'];
             
                
                $data = array( 'username' => $nama, 'status'=>$sts);
                $where = array('id' => $kd);
                // var_dump($data);

                $this->load->model('Mymodel');
                $res = $this->Mymodel->Update('tbl_user', $data, $where);


                $this->session->set_flashdata("msg","
                <div class='alert alert-success alert-block fade in'> 
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>                         
                    <strong> Data Berhasil Dirubah (Edit data sucess !!) </strong>
                </div> 
                ");
                //var_dump($data);
                header('location:'.base_url().'User/tampil_user');
            }else{
                echo "error";
            }
    }

    public function ubah_pass(){
        $kd_user = $this->input->post('NmKd');
        $user = $this->input->post('Nmuser');
        $pswd = $this->input->post('NmPswd');

        $this->db->query("UPDATE tbl_user SET password='$pswd' WHERE kd_user ='$kd_user'");

        $this->session->set_flashdata("msg","
                <div class='alert alert-success fade in'>
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>
                    <strong>Data Berhasil Diubah</strong>
                </div>");       
            
        redirect('Driver','refresh');
    }
}
