<?php
 
class Web extends CI_Controller{
 
    public function __construct(){
        parent::__construct();
        // $this->load->library('form_validation');
        
    }
 
    function index(){
        //$this->load->view('login2');
        $cek = $this->session->userdata('loged_in');
        //var_dump($this->session->userdata('status'));

        if(empty($cek))
        {
            $this->load->view('home_page');
        }else{
            $st = $this->session->userdata('stts');
            if($st=='Admin'){

                header('location:'.base_url().'Admin'); //Admin = nama controler
            }elseif ($st=='User'){
                # code...
                header('location:'.base_url().'User');
            }elseif ($st=='Pimpinan'){
                # code...
                header('location:'.base_url().'Pimpinan');
            }else{
                echo "username dan password salah ...!!";
                header('location:'.base_url().'Web');
            }
        }
    }
 
    function login(){
        

        
        $u = $this->input->post('NmUser');
        $p = $this->input->post('NmPass');
        $this->Mlogin->getLoginData($u,$p);

        
    }
 
    function logout(){
        $cek = $this->session->userdata('logged_in');
        if(empty($cek)){
            header('location:'.base_url().'Web');
        }else{
            $this->session->sess_destroy();
            header('location:'.base_url().'Web');
        }
    }
}
