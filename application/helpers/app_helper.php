<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Modified by grasm for debuging only
 */
if (!function_exists('dd')) {
    /**
     * Dump the passed variables and end the script and exit immediately.
     *
     * @param  mixed
     * @return void
     */
    function dd()
    {
        call_user_func_array('dump', func_get_args());
        die(1);
    }
}

if (!function_exists('dump')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function dump()
    {
        echo "<pre>";
        array_map(function ($x) {
            var_dump($x);
        }, func_get_args());
        echo "</pre>";
    }
}

if (!function_exists('e')) {
    /**
     * Escape HTML special characters in a string.
     *
     * @param  string $value
     * @param bool $return
     * @return string
     */
    function e($value, $return = false)
    {
        $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false);
        if ($return === true)
            return $value;
        else
            echo $value;
    }
}

if (!function_exists('p')) {
    /**
     * Shortcut function for "echo" HTML
     *
     * @param  string $value
     * @param bool $return
     * @return string
     */
    function p($value, $return = false)
    {
        if ($return === true)
            return $value;
        else
            echo $value;
    }
}