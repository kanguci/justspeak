<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Mlogin extends CI_Model{

    //periksa login data pada database

    public function getLoginData($usr,$psw)
    {

        $u = mysql_real_escape_string($usr);
        //$p = md5(mysql_real_escape_string($psw)); //Jika password menggunakan md5
        $p = mysql_real_escape_string($psw);

        $q_cek_login = $this->db->get_where('tbl_user', array('username'=>$u, 'password'=>$p));
        if(count($q_cek_login->result())>0)
        {
            foreach ($q_cek_login->result() as $qck)
            {
                # code...wh
                if($qck->status=='admin')
                {
                    $q_ambil_data = $this->db->get_where('tbl_user', array('username'=>$u));
                    foreach ($q_ambil_data->result() as $qad)
                    {
                        $sess_data['logged_in'] = 'yes';
                        $sess_data['id']   = $qad->id;
                        $sess_data['username']  = $qad->username;
                        $sess_data['status'] = 'admin';
                        $sess_data['password']  = $qad->password;

                        $this->session->set_userdata($sess_data);
                    }
                     header('location:'.base_url().'Admin');
                    

                }else if($qck->status=='user'){
                    $q_ambil_data = $this->db->get_where('tbl_user', array('username'=>$u));
                    foreach ($q_ambil_data->result() as $qad)
                    {
                        
                        $sess_data['logged_in'] = 'yes';
                        $sess_data['id']   = $qad->id;
                        $sess_data['username']  = $qad->username;
                        $sess_data['status'] = 'user';
                        $sess_data['password']  = $qad->password;

                        $this->session->set_userdata($sess_data);
                    }
                     header('location:'.base_url().'User');

                }else if($qck->status=='mhs'){
                    $q_ambil_data = $this->db->get_where('tbl_user', array('username'=>$u));
                    foreach ($q_ambil_data->result() as $qad)
                    {
                        
                        $sess_data['logged_in'] = 'yes';
                        $sess_data['id']   = $qad->id;
                        $sess_data['username']  = $qad->username;
                        $sess_data['status'] = 'mhs';
                        $sess_data['password']  = $qad->password;

                        $this->session->set_userdata($sess_data);
                    }
                     header('location:'.base_url().'Mahasiswa');
                }
            }
        }else
        {
           $this->session->set_flashdata("msg","
                <div class='alert alert-danger fade in'>
                    <a href='#' class='close' data-dismiss='alert'>&times;</a>
                    <i class='fa fa-thumbs-down fa-fw'></i> Username dan Password Salah !
                </div>");

            //echo "<script>alert('Gagal login: Cek username, password!')</script>";
            //header('location:'.base_url().'Login');

            header('location:'.base_url().'Web');

            // echo "<script>alert('Username/password salah, silahkan coba lagi...');";
            // echo "window.location.href = '" .base_url() . "Login';";
            // echo "</script>";

        }
    }
    
}

?>