<?php $this->load->view('header');?>
<?php $this->load->view('side_menu');?>

<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>| Halaman <?php echo $judul ?>
                        <small><?php echo $judul ?></small>
                    </h1>
                </div>
                
            </div>
            <div class="note note-danger">
	            
	            <p> Halaman ini untuk memanipulasi data Soal, termasuk menyimpan, mengubah dan mengahapus data</p>
	        </div>

            <!-- END PAGE BASE CONTENT -->
            

            <!-- END KONTEN -->
            <!-- Halaman Kerja -->
            <?php echo $this->session->flashdata('msg'); ?>
            <div class="row">
            	<div class="col-md-12">
            	<div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Tambah Soal</span>
                        </div>
                        
                    </div>
                    <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Soal_test/simpan" class="form-horizontal form-bordered" method="Post">
                            <!-- <div class="form-body"> -->
                                <div class="form-group last">
                                    <label class="control-label col-md-3">Soal</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor sample" name="txt_soal" id="cke_1_top" rows="6"></textarea>
                                    </div>
                                </div>
                            <!-- </div> -->

                            <!-- <div class="form-body"> -->
                                <div class="form-group last">
                                    <label class="control-label col-md-3">A</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="txt_a" rows="6"></textarea>
                                    </div>
                                </div>
                            <!-- </div> -->
                            <!-- <div class="form-body"> -->
                                <div class="form-group last">
                                    <label class="control-label col-md-3">B</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="txt_b" rows="6"></textarea>
                                    </div>
                                </div>
                            <!-- </div> -->
                            <!-- <div class="form-body"> -->
                                <div class="form-group last">
                                    <label class="control-label col-md-3">C</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="txt_c" rows="6"></textarea>
                                    </div>
                                </div>
                            <!-- </div> -->
                            <!-- <div class="form-body"> -->
                                <div class="form-group last">
                                    <label class="control-label col-md-3">D</label>
                                    <div class="col-md-9">
                                        <textarea class="ckeditor form-control" name="txt_d" rows="6"></textarea>
                                    </div>
                                </div>
                            <!-- </div> -->
                            <div class="form-group">
                                <label class="control-label col-md-3">Kunci Jawaban
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <select class="form-control" name="txt_kunci" >
                                        <option value=""></option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" name="BtnSimpan" id="IdSimpan" class="btn btn dark"><i class="fa fa-save"></i>&nbsp; Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                        </div>

            	</div>

            	
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- Modal Register -->
    
    
    <!-- END CONTENT -->


</div>

<?php $this->load->view('footer');?>

