<?php $this->load->view('header');?>
<?php $this->load->view('side_menu');?>

<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>| Halaman <?php echo $judul ?>
                        <small><?php echo $judul ?></small>
                    </h1>
                </div>
                
            </div>
            
            <div class="note note-info">                        
                <h3 class="">SELAMAT DATANG
                    
                </h3>
            </div>

            <!-- END PAGE BASE CONTENT -->
            <!-- Halaman Kerja -->
            <?php echo $this->session->flashdata('msg'); ?>
            <div class="row">
            	<div class="col-md-12">
            		<div class="portlet light bordered">
            			
            			<div class="portlet-title">
            				<i class="icon-settings font-red-sunglo"></i>
                             <span class="caption-subject bold uppercase"> FORM <?php echo $judul ?></span>
            			</div>
            			<div class="portlet-body form">
            			<!-- <div class="alert alert-danger display-hide">
		                    <button class="close" data-close="alert"></button>
		                    <span> Nidn Dan Nama Tak Boleh Kosong </span>
		                </div> -->

            			<form role="form" method="Post" action="<?php echo base_url()?>Dosen/simpan">

            				<div class="form-body">
            				  <div class="row">
            					<div class="col-md-6">
            					<div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" id="IdNidn" name="NmNidn" required="">
                                    <label for="form_control_1">NIDN</label>                                    
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" id="IdNama" name="NmNama" required="">
                                    <label for="form_control_1">Nama Dosen</label>                                    
                                </div>
                                </div>
                               
                            	<div class="col-md-6">
                            	<div class="form-group form-md-line-input">
									<select class="form-control" id="IdJk" name="NmJk">
										<option value=""></option>
										<option value="Pria">Pria</option>
										<option value="Wanita">Wanita</option>											
									</select>
									<label for="form_control_1">Jenis Kelamin</label>
								</div>									
								</div>
								<div class="col-md-6">
		                        <div class="form-group form-md-line-input">
		                            <input type="text" class="form-control" id="IdHp" name="NmHp">
		                            <label for="form_control_1">Nomor HP</label>                                    
		                        </div>
		                        </div>
		                        <div class="col-md-12">
		                        <div class="form-group form-md-line-input">
		                            <input type="text" class="form-control" id="IdAlm" name="NmAlmt">
		                            <label for="form_control_1">Alamat</label>                                    
		                        </div>	
		                        </div>
		                        <div class="col-md-12">
                            	<div class="form-actions">
										<button type="submit" name="BtnSimpan" id="IdSimpan" class="btn btn dark"><i class="fa fa-save"></i>&nbsp; Simpan</button>

										<button type="submit" class="btn green-meadow" name="BtnEdit" id="IdEdit" disabled=""><i class="fa fa-edit"></i> &nbsp;&nbsp; Edit&nbsp; </button>

										<button type="button" class="btn purple" onclick="window.location.reload() ;"><i class="fa fa-refresh"></i>&nbsp; Refresh </button>
								</div>
                            	</div>
                              </div>
            				</div> 
            				<!-- END FORM BODY -->
            			</form>

            			</div>
            		</div>
            		
            	</div>

            	<div class="col-md-12">
            	<div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Buttons</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th> Nidn </th>
                                <th> Nama Dosen </th>
                                <th> JK </th>
                                <th> Alamat </th>
                                <th> No Hp </th>
                                <th> Hapus </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $g = 0;
								foreach ($dtdsn as $dk){$g++; ?>
								<!-- <tr> -->
								<tr onclick="GetDosen('<?php echo $dk['nidn'];?>','<?php echo $dk['nama_dosen'];?>','<?php echo $dk['jk_dosen'];?>','<?php echo $dk['alamat_dosen'];?>','<?php echo $dk['hp_dosen'];?>')">
									<td><?php echo $dk['nidn']; ?></td>
									<td><?php echo $dk['nama_dosen']; ?></td>
									<td><?php echo $dk['jk_dosen'] ;?></td>
									<td><?php echo $dk['alamat_dosen']; ?></td>
									<td><?php echo $dk['hp_dosen']; ?></td>
									
									<td align="center"> 
										
										<button class="btn btn-xs btn-danger" data-href="<?php echo base_url()."Dosen/hapus/".$dk['nidn'];?>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span></button>

										
									</td>
								</tr>
							<?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>	

            	</div>

            	
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	            
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
	                </div>
	            
	                <div class="modal-body">
	                    <p>Apakah Anda akan menghapus satu Data,..?? </p>
	                    <!-- <p>Apakah Anda ingin melanjutkan?</p> -->
	                    <p class="debug-url"></p>
	                </div>
	                
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                    <a class="btn btn-danger btn-ok btn-md">Hapus</a> 

	                </div>
	            </div>
	        </div>
	    </div>

</div>

<?php $this->load->view('footer');?>

<script type="text/javascript">
	 
	 $("#sample_1").css('cursor', 'pointer');

	$(document).ready(function(){
      setTimeout(function(){
        $(".alert").fadeIn('slow');
      }, 300);
     });
     setTimeout(function(){
        $(".alert").fadeOut('slow');
     }, 2000);

     function GetDosen(nidn,nm,jk,alm,hp){
	 	$("#IdNidn").val(nidn);
	 	$("#IdNama").val(nm);
	 	$("#IdJk").val(jk);
	 	$("#IdAlm").val(alm);
	 	$("#IdHp").val(hp);
	 	// $("#IdStk").val(stk);

	 	document.getElementById('IdEdit').disabled=false;	 	
		document.getElementById('IdSimpan').disabled=true;
		// document.getElementById('IdKode').readOnly=true;
		document.getElementById('IdNidn').readOnly=true;
	}

	$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

      });
</script>