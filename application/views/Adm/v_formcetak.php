<?php $this->load->view('header');?>
<?php $this->load->view('side_menu');?>

		<div id="main" role="main">

			<div id="content">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h1 class="page-title txt-color-blueDark">
							
							<!-- PAGE HEADER -->
							<i class="glyphicon glyphicon-import"></i> 
								<?php echo $judul ?>
							<span>
								| Halaman <?php echo $judul ?>
							</span>
						</h1>
					</div>

				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
						
						<!-- NEW WIDGET START -->
						<article class="col-md-4 col-md-offset-4">

							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-2" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
								<header>
									<h2> Cetak Laporan <?php echo $judul ?> </h2>
								</header>
								
								<!-- widget div-->
								<div>
									<form action="<?php echo base_url();?>Cetak/cetak_lap" id="login-form" class="smart-form client-form" method="Post">
								<header>
									Pilih Jenis Cetak
								</header>

								<fieldset>
									<section>
									<div class="inline-group">
										<label class="radio">
											<input type="radio" name="radio-inline" value="All" checked="">
											<i></i>Cetak Seluruh</label>
										<label class="radio">
											<input type="radio" name="radio-inline" value="Tgl">
											<i></i>Cetak Pertanggal</label>
									</div>
									<!-- <input type="radio" name="time" id="radfull" value="fulltime"> Full Time<br/>
									<input type="radio" name="time" id="radpart" value="parttime"> Part Time -->
								</section>
								<section>
									<label class="label">Tanggal Awal</label>
									<!-- <section class="col col-3"> -->
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
									<input type="text" class="datepicker" data-dateformat='dd-mm-yy' name="nm_tgl1" value="<?php echo date('d-m-Y') ?>" >
									</label>
									
								</section>
								<section>
									<label class="label">Sampai Tanggal</label>
									<!-- <section class="col col-3"> -->
									<label class="input"> <i class="icon-append fa fa-calendar"></i>
									<input type="text" class="datepicker" data-dateformat='dd-mm-yy' name="nm_tgl2" value="<?php echo date('d-m-Y') ?>" >
									</label>
									
								</section>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-danger">
										Cetak
									</button>
								</footer>
							</form>
								</div>
							<!-- end widget -->
						</div>
						

						</article>
						
					</div>

				</section>				

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		
		
<?php $this->load->view('footer'); ?> 

<script type="text/javascript">
	$(document).ready(function() { 
      $('#m_cetak').addClass('active');
      $('#m_in_kl').addClass('active');
    });
</script>

