<?php $this->load->view('header'); ?>
<?php $this->load->view('side_menu'); ?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>| Halaman <?php echo $judul ?>
                    <small><?php echo $judul ?></small>
                </h1>
            </div>

        </div>

        <!-- END PAGE BASE CONTENT -->


        <!-- END KONTEN -->
        <!-- Halaman Kerja -->
        <?php echo $this->session->flashdata('msg'); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Hasil Tes</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="table_peserta">
                            <thead>
                            <tr>
                                <th> Nomor Ujian</th>
                                <th> No Peserta</th>
                                <th> Nama Peserta</th>
                                <th> Kode Soal</th>
                                <th> Benar</th>
                                <th> Salah</th>
                                <th> Nilai</th>
                                <th> Waktu Ujian</th>
                                <th> Last Updated</th>
                                <th style="width: 1px">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($testResults as $i => $item) {?>
                                <tr data-peserta='<?php echo json_encode($item) ?>'>
                                    <td><?php e('U' . str_pad($item->no_ujian, 3, 0, STR_PAD_LEFT))?></td>
                                    <td><?php e($item->no_peserta) ?></td>
                                    <td><?php e($item->nama_peserta) ?></td>
                                    <td><?php e($item->kd_soal) ?></td>
                                    <td><?php e($item->jb_benar) ?></td>
                                    <td><?php e($item->jb_salah) ?></td>
                                    <td><?php e($item->nilai) ?></td>
                                    <td><?php e($item->created_at ? date('d/m/Y H:i', strtotime($item->created_at)) : '-') ?></td>
                                    <td><?php e($item->updated_at ? date('d/m/Y H:i', strtotime($item->updated_at)) : '-') ?></td>
                                    <td align="center">
                                        <?php p(anchor("Test?id=$item->no_ujian", '<i class="glyphicon glyphicon-edit"></i> Edit', ['class' => 'btn btn-xs btn-primary'])) ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>

<!-- END CONTENT -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
            </div>

            <div class="modal-body">
                <p>Apakah Anda akan menghapus satu Data,..?? </p>
                <!-- <p>Apakah Anda ingin melanjutkan?</p> -->
                <p class="debug-url"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok btn-md">Hapus</a>

            </div>
        </div>
    </div>
</div>

</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">


    $("#sample_1").css('cursor', 'pointer');
    // $("#sample_2").css('cursor', 'pointer');

    $(document).ready(function () {
        setTimeout(function () {
            $(".alert").fadeIn('slow');
        }, 300);
    });
    setTimeout(function () {
        $(".alert").fadeOut('slow');
    }, 2000);

    function GetReg(noreg, nm, jk, tmp, alm, hp) {
        $("#Id_Reg").val(noreg);
        $("#id_Nama").val(nm);
        $("#Id_Jk").val(jk);
        $("#id_Tempat").val(tmp);
        $("#Id_Alm").val(alm);
        $("#Id_Hp").val(hp);

    }

    // function GetPeserta(no, nm, tmp, jk, almt, hp, noreg, biaya) {
    //     $("#Id_No").val(no);
    //     $("#id_Nama").val(nm);
    //     $("#id_Tempat").val(tmp);
    //     $("#Id_Jk").val(jk);
    //     $("#Id_Alm").val(almt);
    //     $("#Id_Hp").val(hp);
    //     $("#Id_Reg").val(noreg);
    //     $("#Id_Biaya").val(biaya);
    //
    //     document.getElementById('IdEdit').disabled = false;
    //     document.getElementById('IdSimpan').disabled = true;
    //     document.getElementById('Id_No').readOnly = true;
    //
    // }

    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

    });


    $(document).ready(function () {
        // Js function untuk datepicker
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            todayBtn: true,
            todayHighlight: true
            // startDate: '-3d'
        });
        // Js function untuk event onclick row table peserta
        var $tablePeserta = $('#table_peserta');
        $tablePeserta.on('click', 'tbody tr', function(){
            var data = $(this).data('peserta');
            // console.log(data);
            $("#Id_No").val(data.no_peserta).prop('readonly', true);
            $("#id_Nama").val(data.nama_peserta);
            $("#id_Tempat").val(data.tempat_lahir);
            $("#Id_Jk").val(data.jk_peserta);
            $("#Id_Alm").val(data.alamat_peserta);
            $("#Id_Hp").val(data.no_hp);
            $("#Id_Reg").val(data.no_reg);
            $("#Id_Biaya").val(data.biaya_reg);
            $("#date_of_birth").datepicker('update', data.ttl);
            //
            $('#IdEdit').prop('disabled', false);
            $('#IdSimpan').prop('disabled', true);
        })
    });
</script>