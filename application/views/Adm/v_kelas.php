<?php $this->load->view('header');?>
<?php $this->load->view('side_menu');?>

<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="note note-success">                        
                <h3 class="widget-news-right-body-title">Halaman <?php echo $judul ?>
                    <span class="label label-default"> <?php echo date('d-m-Y') ?> </span>
                </h3>
            </div>

            <!-- END PAGE BASE CONTENT -->
            <!-- Halaman Kerja -->
            <?php echo $this->session->flashdata('msg'); ?>
            <div class="row">
            	<div class="col-md-12">
            		<div class="portlet light bordered">
            			
            			<div class="portlet-title">
            				<i class="icon-settings font-red-sunglo"></i>
                             <span class="caption-subject bold uppercase"> FORM <?php echo $judul ?></span>
            			</div>
            			<div class="portlet-body form">
            			<!-- <div class="alert alert-danger display-hide">
		                    <button class="close" data-close="alert"></button>
		                    <span> Nidn Dan Nama Tak Boleh Kosong </span>
		                </div> -->

            			<form role="form" method="Post" action="<?php echo base_url()?>Kelas/simpan">

            				<div class="form-body">
            				  <div class="row">
            					<div class="col-md-4">
            					<div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" id="id_kode" name="txt_kode" required="" maxlength="10">
                                    <label for="form_control_1">Kode</label>                                    
                                </div>
                                </div>
                                <div class="col-md-4">
                                <div class="form-group form-md-line-input ">
                                    <input type="text" class="form-control" id="id_nama" name="txt_nama" required="">
                                    <label for="form_control_1">Nama Kelas</label>                                    
                                </div>
                                </div>
                             
		                        <div class="col-md-4">
		                        <div class="form-group form-md-line-input ">
		                            <input type="text" class="form-control" id="id_kapasitas" name="txt_kapasitas" required="" onkeypress="return event.charCode >= 48 && event.charCode<= 57">
		                            <label for="form_control_1">Kapasitas</label>                                    
		                        </div>	
		                        </div>
		                        <div class="col-md-12">
                            	<div class="form-actions">
										<button type="submit" name="BtnSimpan" id="IdSimpan" class="btn btn dark"><i class="fa fa-save"></i>&nbsp; Simpan</button>

										<button type="submit" class="btn green-meadow" name="BtnEdit" id="IdEdit" disabled=""><i class="fa fa-edit"></i> &nbsp;&nbsp; Edit&nbsp; </button>

										<button type="button" class="btn purple" onclick="window.location.reload() ;"><i class="fa fa-refresh"></i>&nbsp; Refresh </button>
								</div>
                            	</div>
                              </div>
            				</div> 
            				<!-- END FORM BODY -->
            			</form>

            			</div>
            		</div>
            		
            	</div>

            	<div class="col-md-12">
            	<div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Data <?php echo $judul ?></span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                            <tr>
                                <th> Kode </th>
                                <th> Kelas </th>                               
                                <th> Kapasitas </th>
                                <th> Hapus </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $g = 0;
								foreach ($dtkls as $dk){$g++; ?>
								<!-- <tr> -->
								<tr onclick="GetKls('<?php echo $dk['kd_kelas'];?>','<?php echo $dk['nama_kelas'];?>','<?php echo $dk['kapasitas'];?>')">
									<td><?php echo $dk['kd_kelas']; ?></td>
									<td><?php echo $dk['nama_kelas']; ?></td>
									<td><?php echo $dk['kapasitas']; ?></td>
									<td align="center"> 
										<button class="btn btn-xs btn-danger" data-href="<?php echo base_url()."Kelas/hapus/".$dk['kd_kelas'];?>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span></button>
									</td>
								</tr>
							<?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>	

            	</div>

            	
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	            
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
	                </div>
	            
	                <div class="modal-body">
	                    <p>Apakah Anda akan menghapus satu Data,..?? </p>
	                    <!-- <p>Apakah Anda ingin melanjutkan?</p> -->
	                    <p class="debug-url"></p>
	                </div>
	                
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                    <a class="btn btn-danger btn-ok btn-md">Hapus</a> 

	                </div>
	            </div>
	        </div>
	    </div>

</div>

<?php $this->load->view('footer');?>

<script type="text/javascript">

	$(document).ready(function(){
      setTimeout(function(){
        $(".alert").fadeIn('slow');
      }, 300);
     });
     setTimeout(function(){
        $(".alert").fadeOut('slow');
     }, 2000);

     function GetKls(kd,nm,kap){
	 	$("#id_kode").val(kd);
	 	$("#id_nama").val(nm);
	 	$("#id_kapasitas").val(kap);	 	

	 	document.getElementById('IdEdit').disabled=false;	 	
		document.getElementById('IdSimpan').disabled=true;
		// document.getElementById('IdKode').readOnly=true;
		document.getElementById('id_kode').readOnly=true;
	}

	$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

      });

     $("#sample_1").css('cursor', 'pointer');
     
</script>