<?php $this->load->view('header'); ?>
<?php $this->load->view('side_menu'); ?>

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>| Halaman <?php echo $judul ?>
                    <small><?php echo $judul ?></small>
                </h1>
            </div>

        </div>
        <div class="note note-danger">

            <p> Halaman ini untuk memanipulasi data mahasiswa, termasuk menyimpan, mengubah dan mengahapus data</p>
        </div>

        <!-- END PAGE BASE CONTENT -->


        <!-- END KONTEN -->
        <!-- Halaman Kerja -->
        <?php echo $this->session->flashdata('msg'); ?>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">

                    <div class="portlet-title">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> FORM <?php echo $judul ?></span>
                    </div>
                    <div class="portlet-body form">
                        <!-- <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            <span> Nidn Dan Nama Tak Boleh Kosong </span>
                        </div> -->

                        <form role="form" method="Post" action="<?php echo base_url() ?>Peserta/simpan">

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="Id_No" name="txt_nomor" required="">
                                            <label for="form_control_1">NO Peserta</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="id_Nama" name="txt_nama" required="">
                                            <label for="form_control_1">Nama Peserta</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="id_Tempat" name="txt_tempat" required="">
                                            <label for="form_control_1">Tempat Lahir</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" required="">
                                            <label for="form_control_1">Tanggal Lahir</label>
                                        </div>
                                    </div>
                                    <!--<div class="col-md-4">
                                        <div class="form-group form-md-line-input">
                                            <select class="form-control" id="Id_tgl" name="txt_tgl">
                                                <?php
/*                                                for ($a = 1; $a <= 31; $a += 1) {
                                                    echo "<option value=$a> $a </option>";
                                                }
                                                */?>
                                            </select>
                                            <label for="form_control_1">Tanggal</label>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input">
                                            <select class="form-control" id="Id_Bln" name="txt_bln">
                                                <?php
/*                                                $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                                                $jlh_bln = count($bulan);
                                                for ($c = 0; $c < $jlh_bln; $c += 1) {
                                                    echo "<option value=$bulan[$c]> $bulan[$c] </option>";
                                                }
                                                */?>
                                            </select>
                                            <label for="form_control_1">Bulan</label>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input">
                                            <?php
/*                                            $now = date('Y');
                                            echo "<select class='form-control' name='txt_thn' id='Id_thn'>";
                                            for ($a = 1970; $a <= $now; $a++) {
                                                echo "<option value='$a'>$a</option>";
                                            }
                                            echo "</select>";
                                            */?>
                                            <label for="form_control_1">Tahun</label>
                                        </div>
                                    </div>-->
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <select class="form-control" id="Id_Jk" name="txt_jk">
                                                <option value=""></option>
                                                <option value="L">Pria</option>
                                                <option value="P">Wanita</option>
                                            </select>
                                            <label for="form_control_1">Jenis Kelamin</label>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="Id_Hp" name="txt_hp">
                                            <label for="form_control_1">Nomor HP</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="Id_Alm" name="txt_alamat">
                                            <label for="form_control_1">Alamat</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="Id_Reg" name="txt_noreg" readonly="">
                                            <label for="form_control_1">Nomor Registrasi</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="Id_Biaya" name="txt_biaya">
                                            <label for="form_control_1">Biaya Registrasi</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-actions">
                                            <button type="submit" name="BtnSimpan" id="IdSimpan" class="btn btn dark">
                                                <i class="fa fa-save"></i>&nbsp; Simpan
                                            </button>

                                            <button type="submit" class="btn green-meadow" name="BtnEdit" id="IdEdit" disabled="">
                                                <i class="fa fa-edit"></i> &nbsp;&nbsp; Edit&nbsp;
                                            </button>

                                            <button type="button" class="btn purple" onclick="window.location.reload() ;">
                                                <i class="fa fa-refresh"></i>&nbsp; Refresh
                                            </button>

                                            <div class="nav navbar-nav pull-right">
                                                <!-- btn btn-warning -->
                                                <button type="button" class="btn btn btn-warning" data-target="#datareg" data-toggle="modal">
                                                    <i class="fa fa-edit"></i> &nbsp;&nbsp; Data Registrasi&nbsp;
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END FORM BODY -->
                        </form>

                    </div>
                </div>

            </div>

            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase">Buttons</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="table_peserta">
                            <thead>
                            <tr>
                                <th> Nomor</th>
                                <th> Nama</th>
                                <th> Tempat Lahir</th>
                                <th> Tanggal Lahir</th>
                                <th> JK</th>
                                <th> Alamat</th>
                                <th> No Hp</th>
                                <th> No Registrasi</th>
                                <th> Biaya Admin</th>
                                <th> Hapus</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $g = 0;
                            foreach ($dtpeserta as $dk) {
                                $g++; ?>
                                <!-- <tr> -->
                                <tr data-peserta='<?php echo json_encode($dk) ?>'>
                                    <td><?php echo $dk['no_peserta']; ?></td>
                                    <td><?php echo $dk['nama_peserta']; ?></td>
                                    <td><?php echo $dk['tempat_lahir']; ?></td>
                                    <td><?php echo $dk['ttl']; ?></td>
                                    <td><?php echo $dk['jk_peserta']; ?></td>
                                    <td><?php echo $dk['alamat_peserta']; ?></td>
                                    <td><?php echo $dk['no_hp']; ?></td>
                                    <td><?php echo $dk['no_reg']; ?></td>
                                    <td><?php echo $dk['biaya_reg']; ?></td>

                                    <td align="center">

                                        <button class="btn btn-xs btn-danger" data-href="<?php echo base_url() . "Peserta/hapus/" . $dk['no_peserta']; ?>" data-toggle="modal" data-target="#confirm-delete">
                                            <span class="glyphicon glyphicon-remove"></span></button>


                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- Modal Register -->
<div class="modal fade in" id="datareg" tabindex="-1" role="dialog" aria-hidden="true" style="">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Data Registrasi</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" id="table_registrasi">
                    <thead>
                    <tr>
                        <th> Nomor</th>
                        <th> Tanggal reg</th>
                        <th> Nama</th>
                        <th> tempat Lahir</th>
                        <th> Tanggal Lahir</th>
                        <th> JK</th>
                        <th> Alamat</th>
                        <th> No Hp</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $g = 0;
                    foreach ($dtreg as $dk) {
                        $g++; ?>

                        <tr onclick="GetReg('<?php echo $dk['no_reg']; ?>','<?php echo $dk['nama_reg']; ?>','<?php echo $dk['jk_reg']; ?>','<?php echo $dk['tempat_lahir']; ?>','<?php echo $dk['alamat_reg']; ?>','<?php echo $dk['no_hp']; ?>')" data-dismiss="modal">
                            <!-- <tr> -->
                            <td><?php echo $dk['no_reg']; ?></td>
                            <td><?php echo $dk['tgl_reg']; ?></td>
                            <td><?php echo $dk['nama_reg']; ?></td>
                            <td><?php echo $dk['tempat_lahir']; ?></td>
                            <td><?php echo $dk['tgl_lahir']; ?></td>
                            <td><?php echo $dk['jk_reg']; ?></td>
                            <td><?php echo $dk['alamat_reg']; ?></td>
                            <td><?php echo $dk['no_hp']; ?></td>

                            <!--  <td align="center">
                            
                            <button class="btn btn-xs btn-danger" data-href="<?php //echo base_url()."Dosen/hapus/".$dk['nim'];
                            ?>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span></button>

                            
                        </td> -->
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="button" class="btn green">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END CONTENT -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
            </div>

            <div class="modal-body">
                <p>Apakah Anda akan menghapus satu Data,..?? </p>
                <!-- <p>Apakah Anda ingin melanjutkan?</p> -->
                <p class="debug-url"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok btn-md">Hapus</a>

            </div>
        </div>
    </div>
</div>

</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">


    $("#sample_1").css('cursor', 'pointer');
    // $("#sample_2").css('cursor', 'pointer');

    $(document).ready(function () {
        setTimeout(function () {
            $(".alert").fadeIn('slow');
        }, 300);
    });
    setTimeout(function () {
        $(".alert").fadeOut('slow');
    }, 2000);

    function GetReg(noreg, nm, jk, tmp, alm, hp) {
        $("#Id_Reg").val(noreg);
        $("#id_Nama").val(nm);
        $("#Id_Jk").val(jk);
        $("#id_Tempat").val(tmp);
        $("#Id_Alm").val(alm);
        $("#Id_Hp").val(hp);

    }

    // function GetPeserta(no, nm, tmp, jk, almt, hp, noreg, biaya) {
    //     $("#Id_No").val(no);
    //     $("#id_Nama").val(nm);
    //     $("#id_Tempat").val(tmp);
    //     $("#Id_Jk").val(jk);
    //     $("#Id_Alm").val(almt);
    //     $("#Id_Hp").val(hp);
    //     $("#Id_Reg").val(noreg);
    //     $("#Id_Biaya").val(biaya);
    //
    //     document.getElementById('IdEdit').disabled = false;
    //     document.getElementById('IdSimpan').disabled = true;
    //     document.getElementById('Id_No').readOnly = true;
    //
    // }

    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

    });


    $(document).ready(function () {
        // Js function untuk datepicker
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            todayBtn: true,
            todayHighlight: true
            // startDate: '-3d'
        });
        // Js function untuk event onclick row table peserta
        var $tablePeserta = $('#table_peserta');
        $tablePeserta.on('click', 'tbody tr', function(){
            var data = $(this).data('peserta');
            // console.log(data);
            $("#Id_No").val(data.no_peserta).prop('readonly', true);
            $("#id_Nama").val(data.nama_peserta);
            $("#id_Tempat").val(data.tempat_lahir);
            $("#Id_Jk").val(data.jk_peserta);
            $("#Id_Alm").val(data.alamat_peserta);
            $("#Id_Hp").val(data.no_hp);
            $("#Id_Reg").val(data.no_reg);
            $("#Id_Biaya").val(data.biaya_reg);
            $("#date_of_birth").datepicker('update', data.ttl);
            //
            $('#IdEdit').prop('disabled', false);
            $('#IdSimpan').prop('disabled', true);
        })
    });
</script>