<?php $this->load->view('header');?>
<?php $this->load->view('side_menu');?>

<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>| Halaman <?php echo $judul ?>
                        <small><?php echo $judul ?></small>
                    </h1>
                </div>
                
            </div>
            <div class="note note-danger">
	            
	            <p> Halaman ini untuk memanipulasi data Soal, termasuk menyimpan, mengubah dan mengahapus data</p>
	        </div>

            <!-- END PAGE BASE CONTENT -->
            

            <!-- END KONTEN -->
            <!-- Halaman Kerja -->
            <?php echo $this->session->flashdata('msg'); ?>
            <div class="row">
            	<div class="col-md-12">
            	<div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <!-- <span class="caption-subject bold uppercase">Buttons</span> -->
                        </div>
                        <div class="tools"> </div>
                    </div>                    
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                            <tr>
                                <th> Nomor </th>                                
                                <th> Soal</th>
                                <th> A </th>
                                <th> B </th>                                
                                <th> C </th>
                                <th> D </th>
                                <th> Kunci</th>                                
                                <th> Hapus </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $g = 0;
								foreach ($dtsoal as $dk){$g++; ?>
								<tr>
									<td><?php echo intval($dk['no_soal']); ?></td>
									<td><?php echo $dk['soal'] ;?></td>
									<td><?php echo $dk['opt_a']; ?></td>
                                    <td><?php echo $dk['opt_b'];?></td>
                                    <td><?php echo $dk['opt_c'];?></td>
                                    <td><?php echo $dk['opt_d'];?></td>
                                    <td><?php echo $dk['kunci'];?></td>
									<!-- <td><?php //echo $dk['biaya_reg']; ?></td> -->
									
									<td align="center"> 
										
										<button class="btn btn-xs btn-danger" data-href="<?php echo base_url()."Peserta/hapus/".$dk['no_soal'];?>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span></button>
										
									</td>
								</tr>
							<?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>	
                <div class="note note-danger">
                    <a href="<?php echo base_url()."Soal_test/form_tambah"; ?>" class="btn blue"> Tambah</a>
                    <!-- <button type="button" class="btn blue">Tambah Data</button> -->
                </div>

            	</div>

            	
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
    
    <!-- END CONTENT -->
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	            
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
	                </div>
	            
	                <div class="modal-body">
	                    <p>Apakah Anda akan menghapus satu Data,..?? </p>
	                    <!-- <p>Apakah Anda ingin melanjutkan?</p> -->
	                    <p class="debug-url"></p>
	                </div>
	                
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                    <a class="btn btn-danger btn-ok btn-md">Hapus</a> 

	                </div>
	            </div>
	        </div>
	    </div>

</div>

<?php $this->load->view('footer');?>

<script type="text/javascript">


	 $("#sample_1").css('cursor', 'pointer');
     // $("#sample_2").css('cursor', 'pointer');

	$(document).ready(function(){
      setTimeout(function(){
        $(".alert").fadeIn('slow');
      }, 300);
     });
     setTimeout(function(){
        $(".alert").fadeOut('slow');
     }, 2000);

     function GetReg(noreg,nm,jk,tmp,alm,hp){
	 	$("#Id_Reg").val(noreg);
	 	$("#id_Nama").val(nm);        
	 	$("#Id_Jk").val(jk);
        $("#id_Tempat").val(tmp);
	 	$("#Id_Alm").val(alm);
	 	$("#Id_Hp").val(hp);
	 	
	}

    function GetPeserta(no,nm,tmp,jk,almt,hp,noreg,biaya){
        $("#Id_No").val(no);
        $("#id_Nama").val(nm);
        $("#id_Tempat").val(tmp);
        $("#Id_Jk").val(jk);
        $("#Id_Alm").val(almt);
        $("#Id_Hp").val(hp);
        $("#Id_Reg").val(noreg);
        $("#Id_Biaya").val(biaya);
        
        document.getElementById('IdEdit').disabled=false;       
        document.getElementById('IdSimpan').disabled=true;
        document.getElementById('Id_No').readOnly=true;
        
    }

	$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

      });
</script>