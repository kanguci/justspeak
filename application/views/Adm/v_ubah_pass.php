<!-- <?php $this//->load->view('header'); ?>
<?php $this//->load->view('side_menu'); ?> -->

<div id="main" role="main">

			<!-- MAIN CONTENT -->
	<div id="content" class="container">

		<div class="row">
			
			<div class="col-md-4 col-md-offset-4">
			<div class="row"> 
				<?php echo $this->session->flashdata('msg'); ?>							
			</div>
			<fieldset>
				&nbsp; &nbsp;
			</fieldset>
			
				<div class="well no-padding">
					

					<form action="<?php echo base_url() ?>Ubah_Pass/edit" id="login-form" class="smart-form client-form" method="post">
						<header>
							<p>UPDATE PASSWORD</p>
						</header>

						<fieldset>
							<section>
								<label class="label">ID </label>
								<label class="input"> <i class="icon-append fa fa-group"></i>
									<input type="text" name="NmKd" readonly="" value="<?php echo $this->session->userdata('id')?>">									
								</label>
							</section>
							
							<section>
								<label class="label">User </label>
								<label class="input"> <i class="icon-append fa fa-user"></i>
									<input type="text" name="Nmuser" readonly="" value="<?php echo $this->session->userdata('username')?>">									
								</label>
							</section>

							<section>
									<label class="label">Password</label>						
									<label class="input"> <i class="icon-append fa fa-unlock"></i>
									<input type="password" name="NmPswd" required="">									
								</label> 
								</section>

							<section>
								<!-- <label class="checkbox">
									<input type="checkbox" name="remember" checked="">
									<i></i>Stay signed in</label> -->
									&nbsp;
							</section>
						</fieldset>
						<footer>
							<button type="submit" class="btn btn-primary">
								UPDATE
							</button>
						</footer>
					</form>

				</div>
				<h5 class="text-center"> - Konfirmasi Password -</h5>						
			</div>
		</div>
	</div>

</div>
		
<?php $this->load->view('footer'); ?>

<script type="text/javascript">
	$(document).ready(function() { 
      // $('#m_master').addClass('active');
      $('#m_pass').addClass('active');
    });
</script>
		


