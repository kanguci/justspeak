<?php $this->load->view('header');?>
<?php $this->load->view('side_menu');?> 

		<!-- MAIN PANEL -->
		<div id="main" role="main">

						
			<div id="content">
				<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
					<h1 class="page-title txt-color-blueDark">
						
						<!-- PAGE HEADER -->
						<i class="fa-fw fa fa-user"></i> 
							<?php echo $judul ?>
						<span>
							| Halaman <?php echo $judul ?>
						</span>
					</h1>
				</div>

				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
						
						<!-- NEW WIDGET START -->
						<article class="col-lg-8">

							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-2" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
								<header>
									<h2> Tabel <?php echo $judul ?> </h2>
								</header>
								
								<!-- widget div-->
								<div>
									<!-- widget content -->
									<div class="widget-body no-padding">
												
										<table class="table table-striped table-bordered table-hover" width="100%" id="dt_basic">
											<thead>
												<tr>														
													<th>ID User</th>
													<th>Nama User</th>
													<th>Status</th>													
													<th>Hapus</th>													
												</tr>
											</thead>
											<tbody>

											<!-- foreach ($dtsusut->result_array() as $dk) { ?> -->
											<?php 
												foreach ($dtuser->result_array() as $dk) { ?>

												<tr onclick="GetUsr('<?php echo $dk['id'];?>','<?php echo $dk['username'];?>','<?php echo $dk['status'];?>')">
													<td uppercase><?php echo $dk['id']; ?></td>
													<td><?php echo $dk['username']; ?></td>
													<td><?php echo $dk['status'] ;?></td>												
													<td align="center"> 
														
														<button class="btn btn-xs btn-danger" data-href="<?php echo base_url()."Barang/hapus/".$dk['id'];?>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span></button>
													</td>
												</tr>
												<?php } ?>
												
											</tbody>
										</table>
									
									</div>
							</div>
							<!-- end widget -->
							</div>
						

						</article>
						<!-- WIDGET END -->

						<article class="col-lg-4">

							<!-- Widget ID (each widget will need unique ID)-->
							<?php echo $this->session->flashdata('msg'); ?>

							<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-2" data-widget-colorbutton="false"	data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
								<header>
									<h2> Form Ubah <?php echo $judul ?> </h2>
								</header>
								
								<!-- widget div-->
							<div>							
								<form class="smart-form" method="Post" action="<?php echo base_url() ?>User/simpan">
									<section>
										<label class="label"><b>ID</b></label>
										<label class="input">
											<input type="text" class="input-xs" name="NmKode" id="IdKode" readonly="">
											
										</label>
									</section>

									<section>
										<label class="label"><b>Nama </b></label>
										<label class="input">
											<input type="text" class="input-xs" required="" name="NmNama" id="Idnama" readonly="">
										</label>
									</section>

									<section>
										<label class="label"><b>Status</b></label>
										<label class="select">
											<select class="input-sm" required="" name="NmSts" id="IdSts">
												<option value="admin">Admin</option>
												<option value="user">User</option>												
											</select> <i> </i> </label>
									</section>

									<!-- <div class="form-group">
								      <label class="control-label col-md-2"><b>Status</b></label>
								      <div class="col-sm-12">
								        <select class="form-control" id="IdStat" name="NmStat" required="">
									        <option>Admin</option>
									        <option>User</option>									        			        
									      </select>
								      </div>
								    </div> -->
	
									<fieldset></fieldset>
									<!-- <section></section> -->
									<section>

									<button type="submit" name="BtnEdit" id="IdEdit" class="btn btn-sm btn-warning" disabled=""><span class="glyphicon glyphicon-edit"></span> Ubah Data</button>

									<button type="button" class="btn btn-sm btn-default" onclick="window.location.reload() ;"><span class="glyphicon glyphicon-refresh"></span> Refresh </button>
									</section>
									
								</form>	
							</div>
							<!-- end widget -->
						</div>
						

						</article>
						
					</div>
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	            
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                    <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
	                </div>
	            
	                <div class="modal-body">
	                    <p>Apakah Anda akan menghapus satu Data,..?? </p>
	                    <!-- <p>Apakah Anda ingin melanjutkan?</p> -->
	                    <p class="debug-url"></p>
	                </div>
	                
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                    <a class="btn btn-danger btn-ok btn-md">Hapus</a> 
	                </div>
	            </div>
	        </div>
	    </div>

		<!-- PAGE FOOTER -->
	 <?php $this->load->view('footer'); ?> 


<script type="text/javascript">
	$(document).ready(function() { 
      $('#m_master').addClass('active');
      $('#m_user').addClass('active');
    });

    
</script>