
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Metronic | Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url();?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- <link href="<?php //echo base_url();?>assets/global/plugins/mapplic/mapplic/mapplic.css" rel="stylesheet" type="text/css" /> -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url();?>assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url();?>assets/layouts/layout7/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/layouts/layout7/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />

               

         </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="clearfix">
                <!-- BEGIN BURGER TRIGGER -->
                <div class="burger-trigger">
                    <button class="menu-trigger">
                        <img src="<?php echo base_url();?>assets/layouts/layout7/img/m_toggler.png" alt=""> </button>
                    
                    <div class="menu-bg-overlay">
                        <button class="menu-close">&times;</button>
                    </div>
                    <!-- the overlay element -->
                </div>
                <!-- END NAV TRIGGER -->
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <!-- <a href="#"> -->
                        <img src="<?php echo base_url();?>assets/layouts/layout7/img/logo.png" alt="logo" class="logo-default" /> 
                </div>
                <!-- END LOGO -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <div class="nav navbar-nav pull-right">
                        
                        <!-- tse -->
                        <br>
                        <a class=" btn green btn-outline sbold" data-target="#static" data-toggle="modal"> Login </a>
                        <a class=" btn green btn-outline sbold" data-target="#large" data-toggle="modal"> Register </a>
                    </div>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container page-content-inner page-container-bg-solid">
            <!-- BEGIN BREADCRUMBS -->
            <div class="breadcrumbs">
                <div class="container-fluid">
                    <h2 class="breadcrumbs-title">Dashboard</h2>
                    <ol class="breadcrumbs-list">
                        <!-- <li>                             -->
                            <!-- <a class=" btn green btn-outline sbold" data-target="#static" data-toggle="modal"> Login </a> -->
                        <!-- </li> -->
                       
                    </ol>
                </div>
            </div>

            <!-- Modal -->
            <div id="static" class="modal fade in" tabindex="-1" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">FORM LOGIN</h4>
                            </div>
                            <div class="modal-body">
                                <!-- <?php //echo $this->session->flashdata('msg'); ?> -->
                                <form action="<?php echo base_url();?>Web/login" id="login-form" class="smart-form client-form" method="Post">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-envelope"></i>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Username" name="NmUser" id="IdUsr"> </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <div class="input-group">
                                                <input type="password" class="form-control" id="IdPass" name="NmPass" placeholder="Password">
                                                
                                                <span class="input-group-addon">
                                                    <i class="fa fa-user font-red"></i>
                                                </span>
                                            </div>
                                        </div>      
                                    </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <!-- <button type="button" class="btn default">Cancel</button> -->
                                </div>
                            </form>
                            </div>
                            <div class="modal-footer">
                                <!-- <button type="button" data-dismiss="modal" class="btn dark btn-outline">Cancel</button> -->
                                <!-- <button type="button" data-dismiss="modal" class="btn green">Continue Task</button> -->
                            </div>
                        </div>
                    </div>
            </div>
            <!-- END MODAL -->
            <div class="modal fade in" id="large" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    
                        <div class="modal-header">
                            <div class="note note-success">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">FORM REGISTRASI, SILAHKAN ISI DATA DIRI ANDA !</h4>
                            </div>
                        </div>  
                    <!-- </div>                       -->
                        <div class="modal-body"> 
                        <form method="Post" class="horizontal-form" action="<?php echo base_url()?>Registrasi/generate">                
                            <div class="form-body">
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Nama</label>
                                    <input type="text" class="form-control" placeholder="Masukan Nama " required="" name="txt_nama">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Tempat Lahir</label>
                                    <input type="text" class="form-control" required="" name="txt_tempat">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-2">                                     
                                    <label class="control-label">Tanggal Lahir</label>                                    
                                    <select class="form-control" name="txt_tgl">
                                        <?php
                                        for($a=1; $a<=31; $a+=1){
                                            echo"<option value=$a> $a </option>";
                                        }
                                        ?>                                        
                                    </select>
                                </div>  
                                <div class="form-group col-sm-4">                                     
                                    <label class="control-label">Bulan Lahir</label>                                    
                                    <select class="form-control" name="txt_bln">                                        
                                        <?php
                                        $bulan=array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
                                        $jlh_bln=count($bulan);
                                        for($c=0; $c<$jlh_bln; $c+=1){
                                            echo"<option value=$bulan[$c]> $bulan[$c] </option>";
                                        }
                                        ?>                                      
                                    </select>
                                </div>
                                <div class="form-group col-sm-3">                                     
                                    <label class="control-label">Tahun</label>                                    
                                    <!-- <select class="form-control" name="txt_jk">                                         -->
                                        <?php
                                        $now=date('Y');
                                        echo "<select class='form-control' name='txt_thn'>";
                                        for ($a=1970;$a<=$now;$a++)
                                        {
                                             echo "<option value='$a'>$a</option>";
                                        }
                                        echo "</select>";
                                        ?>
                                    <!-- </select> -->
                                </div>
                                <div class="form-group col-sm-3">
                                        <label class="control-label">Jenis Kelamin</label>
                                        <select class="form-control" name="txt_jk">
                                            <option value=""></option>
                                            <option value="L">Laki Laki</option>
                                            <option value="P">Perempuan</option>                                          
                                        </select>
                                </div>
                                <!-- </div>                                 -->
                                <!-- </div>                                       -->
                            </div> 
                                <!-- </div> -->
                            <!-- </div> -->
                            <div class="row">
                                
                                <div class="form-group col-sm-4">
                                    <label class="control-label">No Handphone </label>
                                    <input type="text" class="form-control" required="" name="txt_hp">
                                </div>
                                <div class="form-group col-sm-8">
                                    <label class="control-label">Alamat</label>
                                    <input type="text" class="form-control" name="txt_alamat">
                                    <!-- <span class="help-block"> A block of help text. </span> -->
                                </div>
                            </div>
                                
                            </div>
                            <div class="form-actions">
                            <section> </section>
                                <button type="submit" class="btn green">Registrasi</button>
                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                        <!-- </div> -->
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                            <button type="button" class="btn green">Save changes</button> -->
                            <!-- <hr> -->
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <!-- BEGIN CONTENT -->
            <div class="container-fluid container-lf-space">
                <!-- BEGIN PAGE BASE CONTENT -->
                <div class="row widget-row">
                <?php echo $this->session->flashdata('msg'); ?>
                    <div class="col-md-6 col-sm-6 col-xs-6 margin-bottom-20">
                        <!-- BEGIN WIDGET WRAP IMAGE -->
                        <div id="carousel-example-generic-v2" class="carousel slide widget-carousel" data-ride="carousel">
                            <!-- Indicators -->
                            
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="widget-wrap-img widget-bg-color-white">
                                        <h3 class="widget-wrap-img-title">New</h3>
                                        <img class="widget-wrap-img-element img-responsive" src="<?php echo base_url();?>assets/layouts/layout7/img/maksu2.jpg" alt=""> </div>
                                </div>
                                <div class="item">
                                    <div class="widget-wrap-img widget-bg-color-white">
                                        <h3 class="widget-wrap-img-title">Maksu</h3>
                                        <img class="widget-wrap-img-element img-responsive" src="<?php echo base_url();?>assets/layouts/layout7/img/maksu1.jpg" alt=""> </div>
                                </div>
                            </div>
                            <ol class="carousel-indicators carousel-indicators-red">
                                <li data-target="#carousel-example-generic-v2" data-slide-to="0" class="circle active"></li>
                                <li data-target="#carousel-example-generic-v2" data-slide-to="1" class="circle"></li>
                            </ol>
                        </div>
                        <!-- END WIDGET WRAP IMAGE -->
                    </div>
                  
                    <div class="col-md-6 col-sm-6 col-xs-6 margin-bottom-20">
                        <!-- BEGIN WIDGET WRAP IMAGE -->
                        <div id="carousel-example-generic-v2" class="carousel slide widget-carousel" data-ride="carousel">
                            <!-- Indicators -->
                            
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="widget-wrap-img widget-bg-color-white">
                                        <h3 class="widget-wrap-img-title">Laut</h3>
                                        <img class="widget-wrap-img-element img-responsive" src="<?php echo base_url();?>assets/layouts/layout7/img/laut.jpg" alt=""> </div>
                                </div>
                                <div class="item">
                                    <div class="widget-wrap-img widget-bg-color-white">
                                        <h3 class="widget-wrap-img-title">Beauty</h3>
                                        <img class="widget-wrap-img-element img-responsive" src="<?php echo base_url();?>assets/layouts/layout7/img/1.jpg" alt=""> </div>
                                </div>
                            </div>
                            <ol class="carousel-indicators carousel-indicators-red">
                                <li data-target="#carousel-example-generic-v2" data-slide-to="0" class="circle active"></li>
                                <li data-target="#carousel-example-generic-v2" data-slide-to="1" class="circle"></li>
                            </ol>
                        </div>
                        <!-- END WIDGET WRAP IMAGE -->
                    </div>
                </div>

                <div class="row widget-bg-color-white no-space margin-bottom-20">
                    <div class="col-md-3 col-sm-6 no-space">
                        <!-- BEGIN WIDGET SUBSCRIBE -->
                        <div class="widget-subscribe widget-subscribe-quote widget-bg-color-purple">
                            <h2 class="widget-subscribe-title widget-title-color-purple-dark text-uppercase">Subscribe
                                <br/> Steps</h2>
                            <p class="widget-subscribe-subtitle widget-title-color-purple-light">Lorem ipsum dolor sit amet diam
                                <a class="widget-subscribe-subtitle-link" href="#">check out</a>
                            </p>
                        </div>
                        <!-- END WIDGET SUBSCRIBE -->
                    </div>
                    <div class="col-md-3 col-sm-6 no-space">
                        <!-- BEGIN WIDGET SUBSCRIBE -->
                        <div class="widget-subscribe">
                            <span class="widget-subscribe-no">1</span>
                            <h2 class="widget-subscribe-title widget-title-color-gray-dark text-uppercase">Important
                                <br/> Step</h2>
                            <p class="widget-subscribe-subtitle widget-title-color-dark-light">Lorem ipsum dolor asqudiete sit amet dolore diam sediate dolor diam
                                <a class="widget-subscribe-subtitle-link" href="#">learn more</a>
                            </p>
                        </div>
                        <!-- END WIDGET SUBSCRIBE -->
                    </div>
                    <div class="col-md-3 col-sm-6 no-space">
                        <!-- BEGIN WIDGET SUBSCRIBE -->
                        <div class="widget-subscribe widget-subscribe-border">
                            <span class="widget-subscribe-no">2</span>
                            <h2 class="widget-subscribe-title widget-title-color-gray-dark text-uppercase">Second
                                <br/> Step</h2>
                            <p class="widget-subscribe-subtitle widget-title-color-dark-light">Lorem ipsum dolor asqudiete sit amet dolore diam sediate psum dolor asqudiete sediat dolor diam
                                <a class="widget-subscribe-subtitle-link" href="#">learn more</a>
                            </p>
                        </div>
                        <!-- END WIDGET SUBSCRIBE -->
                    </div>
                    <div class="col-md-3 col-sm-6 no-space">
                        <!-- BEGIN WIDGET SUBSCRIBE -->
                        <div class="widget-subscribe widget-subscribe-border-top">
                            <span class="widget-subscribe-no">3</span>
                            <h2 class="widget-subscribe-title widget-title-color-gray-dark text-uppercase">Final
                                <br/> Action</h2>
                            <p class="widget-subscribe-subtitle widget-title-color-dark-light">Lorem ipsum dolor asqudiete sit amet dolore diam sediate dolor diam
                                <a class="widget-subscribe-subtitle-link" href="#">learn more</a>
                            </p>
                        </div>
                        <!-- END WIDGET SUBSCRIBE -->
                    </div>
                </div>

                <div class="row widget-row">
                    <div class="col-md-6 margin-bottom-20">
                        <!-- BEGIN WIDGET TAB -->
                        <div class="widget-bg-color-white widget-tab">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab"> All Posts </a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab"> Designers </a>
                                </li>
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab"> Developers </a>
                                </li>
                                <li>
                                    <a href="#tab_1_4" data-toggle="tab"> Others </a>
                                </li>
                            </ul>
                            <div class="tab-content scroller" style="height: 350px;" data-always-visible="1" data-handle-color="#D7DCE2">
                                <div class="tab-pane fade active in" id="tab_1_1">
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/03.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Wondering anyone did this
                                                <span class="label label-default"> March 25 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/04.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">New Workstation
                                                <span class="label label-default"> March 16 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/07.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">San Francisco
                                                <span class="label label-default"> March 10 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/03.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Wondering anyone did this
                                                <span class="label label-default"> March 25 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/04.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">New Workstation
                                                <span class="label label-default"> March 16 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/03.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Wondering anyone did this
                                                <span class="label label-default"> March 25 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/05.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Most Completed theme
                                                <span class="label label-default"> March 12 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_1_2">
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/04.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">New Workstation
                                                <span class="label label-default"> March 16 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/03.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Wondering anyone did this
                                                <span class="label label-default"> March 25 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/05.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Most Completed theme
                                                <span class="label label-default"> March 12 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/07.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">San Francisco
                                                <span class="label label-default"> March 10 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_1_3">
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/05.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Most Completed theme
                                                <span class="label label-default"> March 12 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/07.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">San Francisco
                                                <span class="label label-default"> March 10 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/03.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Wondering anyone did this
                                                <span class="label label-default"> March 25 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/04.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">New Workstation
                                                <span class="label label-default"> March 16 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_1_4">
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/07.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">San Francisco
                                                <span class="label label-default"> March 10 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/04.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">New Workstation
                                                <span class="label label-default"> March 16 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news margin-bottom-20">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/05.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Most Completed theme
                                                <span class="label label-default"> March 12 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                    <div class="widget-news">
                                        <img class="widget-news-left-elem" src="<?php echo base_url();?>assets/layouts/layout7/img/03.jpg" alt="">
                                        <div class="widget-news-right-body">
                                            <h3 class="widget-news-right-body-title">Wondering anyone did this
                                                <span class="label label-default"> March 25 </span>
                                            </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit diam nonumy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END WIDGET TAB -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light tasks-widget widget-comments">
                            <div class="portlet-title margin-bottom-20">
                                <div class="caption caption-md font-red-sunglo">
                                    <span class="caption-subject theme-font bold uppercase">Quick Email</span>
                                </div>
                            </div>
                            <div class="portlet-body overflow-h">
                                <input type="text" placeholder="To" class="form-control margin-bottom-20">
                                <input type="text" placeholder="Subject" class="form-control margin-bottom-20">
                                <textarea placeholder="Message" class="form-control margin-bottom-20" rows="5"></textarea>
                                <button class="btn red-sunglo pull-right" type="button">Submit</button>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 margin-bottom-20">
                        <!-- BEGIN WIDGET PROGRESS -->
                        <div class="widget-progress">
                            <div class="widget-progress-element widget-bg-color-blue margin-bottom-25">
                                <span class="widget-progress-title">Application deplyoment
                                    <span class="pull-right">77%</span>
                                </span>
                                <div class="progress">
                                    <div class="progress-bar widget-bg-color-white" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"> </div>
                                </div>
                            </div>
                            <div class="widget-progress-element widget-bg-color-green margin-bottom-25">
                                <span class="widget-progress-title">Database migration
                                    <span class="pull-right">23%</span>
                                </span>
                                <div class="progress">
                                    <div class="progress-bar widget-bg-color-white" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 23%;"> </div>
                                </div>
                            </div>
                            <div class="widget-progress-element widget-bg-color-red  margin-bottom-25">
                                <span class="widget-progress-title">New UI release
                                    <span class="pull-right">56%</span>
                                </span>
                                <div class="progress">
                                    <div class="progress-bar widget-bg-color-white" role="progressbar" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%;"> </div>
                                </div>
                            </div>
                            <div class="widget-progress-element widget-bg-color-purple">
                                <span class="widget-progress-title">Webserver upgrade
                                    <span class="pull-right">60%</span>
                                </span>
                                <div class="progress">
                                    <div class="progress-bar widget-bg-color-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> </div>
                                </div>
                            </div>
                        </div>
                        <!-- END WIDGET PROGRESS -->
                    </div>
                </div>
                

                <div class="row widget-row no-space margin-bottom-20">
                    <div class="col-md-3 col-sm-6 col-xs-12 no-space">
                        <!-- BEGIN WIDGET SOCIALS -->
                        <div class="widget-socials widget-bg-color-gray">
                            <h2 class="widget-socials-title widget-title-color-white text-uppercase">Metronic
                                <br/> 6 Layout Admin</h2>
                            <div class="margin-bottom-20">
                                <strong class="widget-socials-paragraph text-uppercase">Platform</strong>
                                <a class="widget-socials-paragraph" href="#">Bootstrap Framework</a>
                            </div>
                            <strong class="widget-socials-paragraph text-uppercase">Supports</strong>
                            <a class="widget-socials-paragraph" href="#">SASS Solution</a>
                        </div>
                        <!-- END WIDGET SOCIALS -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 no-space">
                        <!-- BEGIN WIDGET SOCIALS -->
                        <div class="widget-socials widget-gradient" style="background: url(<?php echo base_url();?>assets/layouts/layout7/img/03.jpg)"></div>
                        <!-- END WIDGET SOCIALS -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 no-space">
                        <!-- BEGIN WIDGET SOCIALS -->
                        <div class="widget-socials widget-bg-color-fb">
                            <i class="widget-social-icon-fb icon-social-facebook"></i>
                            <h3 class="widget-social-subtitle">
                                <a href="#">Follow us
                                    <br/> on Facebook</a>
                            </h3>
                        </div>
                        <!-- END WIDGET SOCIALS -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 no-space">
                        <!-- BEGIN WIDGET SOCIALS -->
                        <div class="widget-socials widget-bg-color-tw">
                            <i class="widget-social-icon-tw icon-social-twitter"></i>
                            <h3 class="widget-social-subtitle">
                                <a href="#">Follow us
                                    <br/> on Twitter</a>
                            </h3>
                        </div>
                        <!-- END WIDGET SOCIALS -->
                    </div>
                </div>
                
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN QUICK SIDEBAR -->
        <a href="javascript:;" class="page-quick-sidebar-toggler">
            <i class="icon-login"></i>
        </a>
        <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
            <div class="page-quick-sidebar">
                <ul class="nav nav-tabs">
                  
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
                        <div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list">
                           
                        </div>
                       
                    </div>
                    
                    
                </div>
            </div>
        </div>

        <!-- modal -->
        
        
        <!-- END QUICK SIDEBAR -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner container-fluid container-lf-space">
                <p class="page-footer-copyright">2015 &copy; Metronic by keenthemes.
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </p>
            </div>
            <div class="go2top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
        
        <!-- END QUICK SIDEBAR TOGGLER -->
        <!--[if lt IE 9]>
<script src="<?php //echo base_url();?>assets/global/plugins/respond.min.js"></script>
<script src="<?php //echo base_url();?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url();?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>        
        <script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url();?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        
        
        
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS 
        <script src="<?php //echo base_url();?>assets/layouts/layout7/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php //echo base_url();?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript">
            $(document).ready(function(){
              setTimeout(function(){
                $(".alert").fadeIn('slow');
              }, 300);
             });
             setTimeout(function(){
                $(".alert").fadeOut('slow');
             }, 2000);
        </script>
        
    </body>

    

</html>