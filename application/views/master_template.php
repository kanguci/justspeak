<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>| Halaman <?php echo $judul ?>
                        <small><?php echo $judul ?></small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                
                
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="note note-info">                        
                <h3 class="widget-news-right-body-title">SELAMAT DATANG
                    <span class="label label-default"> March 25 </span>
                </h3>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>
    
    <!-- END QUICK SIDEBAR -->
</div>