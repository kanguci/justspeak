<div class="page-sidebar-wrapper">
                
    <div class="page-sidebar navbar-collapse collapse">
        
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="index.html" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Dashboard 1</span>
                        </a>
                    </li>
                                                    
                </ul>
            </li>
            <li class="heading">
                <h3 class="uppercase">Mahasiswa</h3>
            </li>
            
            <?php
                # Menu Soal --------------------------------------------------------------------------------
                // list sub menu
                $routeSoal = [
                    // 'Soal_test' => 'Soal Test',
                    'Test' => 'Soal Test',
                    // 'Test/hasil' => 'Hasil Test',
                ];
                // check menu active
                $parentActive = in_array(uri_string(), array_keys($routeSoal));
            ?>
            <li class="nav-item <?php e($parentActive ? 'active open' : '') ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-puzzle"></i>
                    <span class="title">Page Test</span>
                    <?php if($parentActive): ?>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    <?php else: ?>
                        <span class="arrow"></span>
                    <?php endif; ?>
                </a>
                <ul class="sub-menu">
                    <!-- Looping sub menu -->
                    <?php foreach($routeSoal as $route => $label): ?>
                    <?php
                        // check sub menu active
                        $childActive = strcasecmp(uri_string(), $route) == 0;
                    ?>
                    <li class="nav-item <?php e($childActive ? 'active open' : '') ?>">
                        <a href="<?php e(base_url($route))?>" class="nav-link ">
                            <span class="title"><?php e($label) ?></span>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
                <!-- END SIDEBAR -->
</div>