<?php $this->load->view('header'); ?>
<?php $this->load->view('side_menu'); ?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->

        <div class="note note-info">
            <h3 class="widget-news-right-body-title">SELAMAT DATANG
                <span class="label label-default"> Halaman <?php echo $judul ?> Jumlah Soal <?php echo $Jml ?> </span>
            </h3>
        </div>
        <!-- END PAGE BASE CONTENT -->
        <!-- <div class="row">
        <div class="border-red-thunderbird margin-bottom-5 bg-white" style="padding: 10px; border: 2px solid #fff;"> </div>
        </div> -->
        <div class="portlet light bordered">


            <div class="portlet-body form">
                <?php /*dump($dttes)*/ ?>
                <form role="form" method="Post" action="<?php echo base_url() ?>Test/<?php e($dttes ? "$dttes->no_ujian/update" : 'simpan')?>">
                    <!-- <form  method="POST" id="frm-filter" class="form-group"> -->

                    <?php $g = 0;
                    foreach ($dtsoal as $dk) {
                        $g++; ?>
                        <div class="form-body">
                            <div class="form-group">
                                <label name="txt_nosoal"><?php echo intval($dk['no_soal']); ?></label>.&nbsp;
                                <label><?php echo $dk['soal']; ?></label>
                                <!--input type="hidden" class="form-control" id="Id_no_soal" value="<?php echo intval($dk['no_soal']); ?>" name="txt_nomor"-->
                                <!-- <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input type="text" class="form-control" placeholder="Email Address"> </div> -->
                            </div>
                            <div class="form-group">
                                <div class="radio-list">
                                    <?php foreach (range('A', 'D') as $option): ?>
                                        <label>
                                            <input type="radio" name="jawab[<?php e(intval($dk['no_soal'])) ?>]" id="Rb_Jawab_<?php e(intval($dk['no_soal'])) ?>" value="<?php e($option) ?>" <?php /*e($dttes ? 'disabled' : '') */?> <?php e(isset($answers[intval($dk['no_soal'])]) && $answers[intval($dk['no_soal'])] == $option ? 'checked' : '') ?>><?php e($option) ?>
                                            .&nbsp;
                                            <label><?php p($dk['opt_' . strtolower($option)]) ?></label>
                                        </label>
                                    <?php endforeach; ?>

                                    <?php /**
                                     * <!-- <div class="col-md-6"> -->
                                     * <label>
                                     * <input type="radio" name="jawab[<?php echo intval($dk['no_soal']); ?>]" id="Rb_Jawab" value="A">A.&nbsp;
                                     * <label><?php echo $dk['opt_a']; ?></label>
                                     * </label>
                                     * <!-- </div>
                                     * <div class="col-md-6"> -->
                                     * <label>
                                     * <input type="radio" name="jawab[<?php echo intval($dk['no_soal']); ?>]" id="Rb_Jawab" value="B">B.&nbsp;
                                     * <label><?php echo $dk['opt_b']; ?></label>
                                     * </label>
                                     * <!-- </div>
                                     * <div class="col-md-6"> -->
                                     * <label>
                                     * <input type="radio" name="jawab[<?php echo intval($dk['no_soal']); ?>]" id="Rb_Jawab" value="C">C.&nbsp;
                                     * <label><?php echo $dk['opt_c']; ?> </label>
                                     * </label>
                                     * <!--  </div>
                                     * <div class="col-md-6"> -->
                                     * <label>
                                     * <input type="radio" name="jawab[<?php echo intval($dk['no_soal']); ?>]" id="Rb_Jawab" value="D">D.&nbsp;
                                     * <label><?php echo $dk['opt_d']; ?> </label>
                                     * </label>
                                     */ ?>
                                </div>
                            </div>
                        </div>
                        <hr>
                    <?php } ?>
                    <div class="form-actions">
                        <button type="submit" name="BtnSimpan" id="IdSimpan" class="btn btn dark">
                            <i class="fa fa-save"></i>&nbsp; <?php e($dttes ? 'Update' : 'Simpan') ?>
                        </button>
                    </div>
                </form>

            </div>


        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
    //  $(document).ready(function(){
    //     $("#personal-info").submit(function(e){
    //         e.preventDefault();
    //         var Jawab = $("#Rb_Jawab").val();;
    //         var soal= $("#Id_no_soal").val();
    //         $.ajax({
    //             type: "POST",
    //             url: '<?php echo base_url() ?>Test/simpan',
    //             data: {jb_tes:Jawab,no_soal:soal},
    //             success:function(data)
    //             {
    //                 alert('SUCCESS!!');
    //             },
    //             error:function()
    //             {
    //                 alert('fail');
    //             }
    //         });
    //     });
    // });

    $('#frm-filter').submit(function (event) {
        dataString = $("#frm-filter").serialize();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Test/simpan",
            data: dataString,

            success: function (msg) {
                //$('#respon').html(msg);
            },
        });
        event.preventDefault();
    });
</script>
