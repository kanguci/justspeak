<?php $this->load->view('header');?>
<?php $this->load->view('side_menu');?>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
        
            <div class="note note-info">                        
                <h3 class="widget-news-right-body-title">SELAMAT DATANG
                    <span class="label label-default"> Halaman <?php echo $judul ?> </span>
                </h3>
            </div>
            <!-- END PAGE BASE CONTENT -->
            <div class="row">
            <div class="border-red-thunderbird margin-bottom-5 bg-white" style="padding: 10px; border: 2px solid #fff;"> </div>
            </div>

                <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-share font-green-sharp"></i>
                                <span class="caption-subject font-green-sharp bold uppercase">Soal</span>
                            </div>
                            
                        </div>

                        <table width="100%">
                        <?php
                            $ceksoal = $this->db->get("tbl_soal_tes");
                            $no=0;
                            foreach($ceksoal->result() as $rs){
                                $no++;
                        ?>
                        
                        <form role="form" method="Post" action="<?php echo base_url()?>Dosen/simpan">
                        <div class="form-body">
                        <!-- <table width="100%"> -->
                            <tr>
                                <td width="5%"><?php echo $no ?></td>
                                <td colspan="2"><?php echo $rs->soal ?></td>
                            </tr>
                            <div class="form-group"> 
                            <div class="radio-list">
                            <tr>
                                
                                <td></td>
                                <td>A.<input type="radio" value="A" name="radio"> <label><?php echo $rs->opt_a; ?></label> 

                                </td>
                                <!-- </div> -->
                            </tr>

                            <tr>
                               <td></td>
                               <td>B.<input type="radio" value="B" name="radio"> <label><?php echo $rs->opt_b; ?></label></td> 
                            </tr>
                            <tr>
                               <td></td>
                               <td>C.<input type="radio" value="C" name="radio"> <label><?php echo $rs->opt_c; ?></label></td> 
                            </tr>
                            <tr>
                                <td></td>
                                
                                <td>D.<input type="radio" value="D" name="radio"> <label><?php echo $rs->opt_d; ?></label></td>
                            </tr>
                            </div>
                            </div>
                       
                        
                        </div>
                        </form>
                        <?php } ?>

                         </table>
 </div>
    <!-- END CONTENT -->
</div>
</div>

<?php $this->load->view('footer');?>