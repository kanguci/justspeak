<?php $this->load->view('header');?>
            <!-- BEGIN SIDEBAR -->
<?php $this->load->view('side_menu');?>
            <!-- END SIDEBAR -->
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>| Halaman <?php echo $judul ?>
                <small><?php echo $judul ?></small>
            </h1>
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="note note-info">                        
        <h3 class="widget-news-right-body-title">SELAMAT DATANG LEMBAGA KURSUS JUSTSPEAK, SILAHKAN ISI DATA DIRI ANDA</h3>
    </div>
    <!-- <div class="note note-warning"> -->
    <?php echo $this->session->flashdata('msg'); ?>
        <div class="panel panel-default">
            <div class="panel-heading"> FORM REGISTRASI </div>            
            <div class="panel-body">
                <form method="Post" action="<?php echo base_url()?>Registrasi/generate">
                
                <div class="form-body">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="control-label">Nama</label>
                        <input type="text" class="form-control" placeholder="Masukan Nama " required="" name="txt_nama">
                        <span class="help-block"> A block of help text. </span>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="control-label">Tanggal Lahir</label>
                        
                       <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <input type="text" class="form-control" readonly="" name="dtp_tgl">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label class="control-label">Jenis Kelamin</label>
                        <!-- <div class="input-group"> -->
                            <select class="form-control" name="txt_jk">
                                <option value=""></option>
                                <option value="L">Laki Laki</option>
                                <option value="P">Perempuan</option>                                          
                            </select>
                        <!-- </div> -->
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="control-label">No Handphone </label>
                        <input type="text" class="form-control" required="" name="txt_hp">
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="control-label">Alamat</label>
                        <input type="text" class="form-control" name="txt_alamat">
                        <!-- <span class="help-block"> A block of help text. </span> -->
                    </div>
                </div>
                    
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn green">Registrasi</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
            </form>

            </div>
        </div>

    </div>
    <!-- END PAGE BASE CONTENT -->
<!-- </div> -->
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="icon-login"></i>
</a>
    
    <!-- END QUICK SIDEBAR -->
</div>
        <!-- BEGIN FOOTER -->
<?php $this->load->view('footer');?>