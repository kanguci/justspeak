/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.13-MariaDB : Database - db_scele
=======
MySQL - 5.6.20 : Database - db_scele
>>>>>>> 0ffcbcc4374547693614c675c5c55637f842184f
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tbl_admin` */

DROP TABLE IF EXISTS `tbl_admin`;

CREATE TABLE `tbl_admin` (
  `username` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_admin` */

/*Table structure for table `tbl_bobot` */

DROP TABLE IF EXISTS `tbl_bobot`;

CREATE TABLE `tbl_bobot` (
  `bobot` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `nilai` char(5) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `tbl_bobot` */

/*Table structure for table `tbl_dosen` */

DROP TABLE IF EXISTS `tbl_dosen`;

CREATE TABLE `tbl_dosen` (
  `nidn` varchar(10) NOT NULL,
  `nama_dosen` varchar(30) DEFAULT NULL,
  `jk_dosen` char(8) DEFAULT NULL,
  `alamat_dosen` varchar(50) DEFAULT NULL,
  `hp_dosen` char(15) DEFAULT NULL,
  PRIMARY KEY (`nidn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_dosen` */

insert  into `tbl_dosen`(`nidn`,`nama_dosen`,`jk_dosen`,`alamat_dosen`,`hp_dosen`) values 
('01012008','Agus Mulyanto. S.Kom','Wanita','Jl. Rajabasa Kp. Keramat No.35 Gg. Ratu','0821 3345 6690'),
('01092010','Sunah Saputri','Wanita','Jl. Rajabasa Kp. Keramat No.35','0821 3345 6625'),
('02102012','Jupriyasi, M.Kom.','Pria','Jl. Pramuka','0821 3345 5565');

/*Table structure for table `tbl_hasil_tes` */

DROP TABLE IF EXISTS `tbl_hasil_tes`;

CREATE TABLE `tbl_hasil_tes` (
  `kode` smallint(3) NOT NULL AUTO_INCREMENT,
  `jb_tes` enum('A','B','C','D') DEFAULT NULL,
  `no_soal` char(3) DEFAULT NULL,
  `no_peserta` char(10) DEFAULT NULL,
  `kd_soal` int(10) unsigned DEFAULT NULL,
  `no_ujian` int(10) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_hasil_tes` */

insert  into `tbl_hasil_tes`(`kode`,`jb_tes`,`no_soal`,`no_peserta`,`kd_soal`,`no_ujian`) values 
(1,'A','1','adm001',NULL,NULL),
(2,'B','2','adm001',NULL,NULL),
(3,'C','3','adm001',NULL,NULL),
(4,'C','4','adm001',NULL,NULL),
(5,'D','5','adm001',NULL,NULL),
(6,'B','6','adm001',NULL,NULL),
(7,'C','7','adm001',NULL,NULL),
(8,'B','8','adm001',NULL,NULL),
(9,'C','1','adm001',NULL,4),
(10,'C','2','adm001',NULL,4),
(11,'C','3','adm001',NULL,4),
(12,'D','4','adm001',NULL,4),
(13,'B','5','adm001',NULL,4),
(14,'C','6','adm001',NULL,4),
(15,'B','7','adm001',NULL,4),
(16,'B','8','adm001',NULL,4),
(17,'A','1',NULL,NULL,5),
(18,'C','2',NULL,NULL,5),
(19,'C','3',NULL,NULL,5),
(20,'A','4',NULL,NULL,5),
(21,'B','5',NULL,NULL,5),
(22,'D','6',NULL,NULL,5),
(23,'B','7',NULL,NULL,5),
(24,'B','8',NULL,NULL,5),
(25,'A','1',NULL,NULL,6),
(26,'A','2',NULL,NULL,6),
(27,'B','3',NULL,NULL,6),
(28,'C','4',NULL,NULL,6),
(29,'C','5',NULL,NULL,6),
(30,'D','6',NULL,NULL,6),
(31,'B','7',NULL,NULL,6),
(32,'B','8',NULL,NULL,6),
(33,'A','1',NULL,NULL,13),
(34,'A','2',NULL,NULL,13),
(35,'C','3',NULL,NULL,13),
(36,'C','4',NULL,NULL,13),
(37,'D','5',NULL,NULL,13),
(38,'B','6',NULL,NULL,13),
(39,'C','7',NULL,NULL,13),
(40,'B','8',NULL,NULL,13),
(41,'B','1',NULL,NULL,14),
(42,'D','2',NULL,NULL,14),
(43,'B','3',NULL,NULL,14),
(44,'C','4',NULL,NULL,14),
(45,'C','5',NULL,NULL,14),
(46,'A','7',NULL,NULL,14),
(47,'B','8',NULL,NULL,14);

/*Table structure for table `tbl_jadwal` */

DROP TABLE IF EXISTS `tbl_jadwal`;

CREATE TABLE `tbl_jadwal` (
  `kd_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `kd_mk` varchar(20) NOT NULL,
  `kd_dosen` varchar(5) NOT NULL,
  `kd_tahun` varchar(20) NOT NULL,
  `jadwal` varchar(100) NOT NULL,
  `kapasitas` int(3) NOT NULL,
  `kelas_program` varchar(10) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  PRIMARY KEY (`kd_jadwal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_jadwal` */

/*Table structure for table `tbl_jurusan` */

DROP TABLE IF EXISTS `tbl_jurusan`;

CREATE TABLE `tbl_jurusan` (
  `kd_jurusan` varchar(4) NOT NULL,
  `nama_jurusan` varchar(30) NOT NULL,
  PRIMARY KEY (`kd_jurusan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_jurusan` */

insert  into `tbl_jurusan`(`kd_jurusan`,`nama_jurusan`) values 
('RPL','REKAYASA PERANGKAT LUNAK'),
('TKJ','TEKNIK KOMPUTER JARINGAN');

/*Table structure for table `tbl_kelas` */

DROP TABLE IF EXISTS `tbl_kelas`;

CREATE TABLE `tbl_kelas` (
  `kd_kelas` char(10) NOT NULL,
  `nama_kelas` varchar(50) DEFAULT NULL,
  `kapasitas` int(3) DEFAULT NULL,
  PRIMARY KEY (`kd_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kelas` */

insert  into `tbl_kelas`(`kd_kelas`,`nama_kelas`,`kapasitas`) values 
('MK-001','Ilmu pengetahuan alam',NULL),
('SP-001','Sasing',35),
('WR-001','Writing',40);

/*Table structure for table `tbl_mhs` */

DROP TABLE IF EXISTS `tbl_mhs`;

CREATE TABLE `tbl_mhs` (
  `nim` char(20) NOT NULL,
  `nama_mhs` varchar(50) NOT NULL,
  `jk` enum('L','P') NOT NULL DEFAULT 'L',
  `agama` varchar(20) DEFAULT NULL,
  `alamat_mhs` varchar(50) NOT NULL,
  `no_hp` char(15) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_mhs` */

/*Table structure for table `tbl_mk` */

DROP TABLE IF EXISTS `tbl_mk`;

CREATE TABLE `tbl_mk` (
  `kd_mk` varchar(10) NOT NULL DEFAULT '',
  `nama_mk` varchar(100) DEFAULT NULL,
  `jum_sks` int(2) DEFAULT NULL,
  `semester` int(2) DEFAULT NULL,
  `prasyarat_mk` varchar(50) DEFAULT NULL,
  `kode_jur` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`kd_mk`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tbl_mk` */
insert  into `tbl_mk`(`kd_mk`,`nama_mk`,`jum_sks`,`semester`,`prasyarat_mk`,`kode_jur`) values 
('PBO','Pemrograman Berbasis Objek',NULL,NULL,NULL,NULL),
('IPA','Ilmu Pengetahuan Alam',NULL,NULL,NULL,NULL);
/*Table structure for table `tbl_nilai` */

DROP TABLE IF EXISTS `tbl_nilai`;

CREATE TABLE `tbl_nilai` (
  `nim` varchar(20) NOT NULL,
  `kd_mk` varchar(50) NOT NULL,
  `kd_dosen` varchar(20) NOT NULL,
  `kd_tahun` varchar(20) NOT NULL,
  `semester_ditempuh` int(2) NOT NULL,
  `grade` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tbl_nilai` */

/*Table structure for table `tbl_peserta` */

DROP TABLE IF EXISTS `tbl_peserta`;

CREATE TABLE `tbl_peserta` (
  `no_peserta` char(10) NOT NULL,
  `nama_peserta` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `ttl` varchar(50) DEFAULT NULL,
  `jk_peserta` enum('L','P') DEFAULT NULL,
  `alamat_peserta` varchar(100) DEFAULT NULL,
  `no_hp` char(15) DEFAULT NULL,
  `no_reg` char(10) NOT NULL,
  `biaya_reg` int(11) DEFAULT NULL,
  PRIMARY KEY (`no_peserta`),
  KEY `m1` (`no_reg`),
  CONSTRAINT `m1` FOREIGN KEY (`no_reg`) REFERENCES `tbl_registrasi` (`no_reg`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_peserta` */

insert  into `tbl_peserta`(`no_peserta`,`nama_peserta`,`tempat_lahir`,`ttl`,`jk_peserta`,`alamat_peserta`,`no_hp`,`no_reg`,`biaya_reg`) values 
('25463452','Agus','Bandung','2018-08-16','L','Alamat','0897','DF-0000001',23456234),
('P-0001','Royani','Jakarta','2000-09-15','L','Jl. Diponogor0 Rt.09/001 No. 15 Teluk Betung Barat','0859 2009 0099','DF-0000003',100000),
('P-001','Ningsih','Indramayu','1995-07-02','P','Jalan Raya Bogor no.09','0878 3332 XXXX','DF-0000002',100000);

/*Table structure for table `tbl_registrasi` */

DROP TABLE IF EXISTS `tbl_registrasi`;

CREATE TABLE `tbl_registrasi` (
  `no_reg` char(10) NOT NULL,
  `tgl_reg` date DEFAULT NULL,
  `nama_reg` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(30) DEFAULT NULL,
  `jk_reg` enum('L','P') DEFAULT NULL,
  `alamat_reg` varchar(50) DEFAULT NULL,
  `no_hp` char(15) DEFAULT NULL,
  PRIMARY KEY (`no_reg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_registrasi` */

insert  into `tbl_registrasi`(`no_reg`,`tgl_reg`,`nama_reg`,`tempat_lahir`,`tgl_lahir`,`jk_reg`,`alamat_reg`,`no_hp`) values 
('DF-0000001','2018-08-02','Agus','Bandung','12 Agustus 1987','L','Alamat','0897'),
('DF-0000002','2018-08-02','Ningsih','Indramayu','29 Oktober 1995','P','Jl. antasari no 12 bandar lampung','0819 3345 6132'),
('DF-0000003','2018-08-04','Royani','Jakarta','14 Mei 1989','L','Jl. Ikan Bawal no 12 Teluk betung selatan','0893 4456 212'),
('DF-0000004','2018-08-04','Nama Peserta','Tempat Lahir Peserta','10 Januari 1980','L','Alamat Peserta','No Handphone');

/*Table structure for table `tbl_soal_tes` */

DROP TABLE IF EXISTS `tbl_soal_tes`;

CREATE TABLE `tbl_soal_tes` (
  `no_soal` char(3) NOT NULL,
  `soal` longtext,
  `opt_a` text,
  `opt_b` text,
  `opt_c` text,
  `opt_d` text,
  `kunci` enum('A','B','C','D') DEFAULT NULL,
  PRIMARY KEY (`no_soal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_soal_tes` */

insert  into `tbl_soal_tes`(`no_soal`,`soal`,`opt_a`,`opt_b`,`opt_c`,`opt_d`,`kunci`) values 
('001','<p>1</p>\r\n','<p>1</p>\r\n','<p>1</p>\r\n','<p>1</p>\r\n','<p>1</p>\r\n','A'),
('002','<p>Siapakah Nama Adik Budi ?</p>\r\n','<p>Iwan</p>\r\n','<p>Nama</p>\r\n','<p>Ilham</p>\r\n','<p>Andi</p>\r\n','A'),
('003','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Pimpinan dari Gerakan DI/TII Jawa Tengah, saat itu menjabat sebagai ....</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Komandan Laskar Hisbullah di front Tulangan, Sidoarjo, dan Mojokerto</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Komandan Laskar Hisbullah di front Brebes, Tegal, dan Pekalongan</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">&nbsp;Komandan Laskar Hisbullah di front Aceh, Jawa Tengah, dan Jawa Barat</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Komandan Laskar Hisbullah di frontBrebes,Sidoarjo, dan Mojokerto</span></p>\r\n','A'),
('004','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Latar belakang terjadinya Pemberontakan Andi Azis adalah</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">tidak menyetujui Indonesia timur bergabung ke NKRI</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">ketidakpuasan daerah terhadap alokasi biaya dan pembangunan dari pusat</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">bergabung ke Negara Islam Kartosuwiryo</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">menolak masuknya pasukan APRIS dari TNI ke Sulawesi Selatan</span></p>\r\n','A'),
('005','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Untuk menumpas PRRI, pemerintah dan KSAD memutaskan untuk melancarkan operasi militer. Operasi militer ini diberi nama operasi ....</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Pagar Betis</span></p>\r\n','<p>Baratayudha</p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Banteng Raiders</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">17 Agustus</span></p>\r\n','D'),
('006','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Munculnya Dewan Banteng, Dewan Gajah, dan Dewan Garuda disebabkan oleh ....</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">persaingan antara anggota ABRI dan</span><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">&nbsp;perebutan jabatan di Sumatra Barat</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">munculnya kelas-kelas dalam masyarakat di Selawesi Selatan</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">perebutan jabatan Kapolres di sebuah kabupaten di Selawesi Selatan</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">ketidakpuasan beberapa di Sumatra dan Sulawesi terhadap dana pembangunan dan pemerintahan pusat</span></p>\r\n','D'),
('007','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Usaha pemerintah dalam mengatasi Gerakan DI/TII Kartosuwiryo adalah ....</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">pembersihan terhadap antek-antek Kartosuwiryo</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">musyawarah dan pengarahan pasukan TNI</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">penyerangan terhadap Kartosuwiryo</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">penangkapan Kartosuwiryo</span></p>\r\n','B'),
('008','<p>15 X 2 = ...</p>\r\n','<p>30</p>\r\n','<p>20</p>\r\n','<p>65</p>\r\n','<p>17</p>\r\n','A');

/*Table structure for table `tbl_tes` */

DROP TABLE IF EXISTS `tbl_tes`;

CREATE TABLE `tbl_tes` (
  `no_ujian` int(11) NOT NULL AUTO_INCREMENT,
  `kd_soal` char(3) DEFAULT NULL,
  `no_peserta` char(10) DEFAULT NULL,
  `jb_benar` smallint(5) DEFAULT '0',
  `jb_salah` smallint(5) DEFAULT '0',
  `nilai` decimal(7,2) DEFAULT '0.00',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`no_ujian`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tes` */

insert  into `tbl_tes`(`no_ujian`,`kd_soal`,`no_peserta`,`jb_benar`,`jb_salah`,`nilai`,`updated_at`,`created_at`) values 
(4,'A','adm001',0,0,0.00,NULL,'2018-08-16 11:00:11'),
(5,'A','adm001',0,0,0.00,NULL,'2018-08-16 11:00:14'),
(6,'A','adm001',0,0,0.00,NULL,'2018-08-16 11:00:17'),
(13,'C','adm001',3,5,37.50,'2018-08-16 07:02:46','2018-08-16 07:02:46'),
(14,'A','adm001',0,7,0.00,'2018-08-16 07:13:07','2018-08-16 07:13:07');

/*Table structure for table `tbl_thn_ajaran` */

DROP TABLE IF EXISTS `tbl_thn_ajaran`;

CREATE TABLE `tbl_thn_ajaran` (
  `kd_tahun` varchar(20) NOT NULL,
  `keterangan` varchar(20) DEFAULT NULL,
  `tgl_kul` varchar(20) DEFAULT NULL,
  `tgl_awal_perwalian` varchar(20) DEFAULT NULL,
  `tgl_akhir_perwalian` varchar(20) DEFAULT NULL,
  `stts` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kd_tahun`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_thn_ajaran` */

/*Table structure for table `tbl_upload` */

DROP TABLE IF EXISTS `tbl_upload`;

CREATE TABLE `tbl_upload` (
  `kd_upload` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kd_mk` char(10) DEFAULT NULL,
  `tgl_upload` date DEFAULT NULL,
  `nama_file` varchar(100) DEFAULT NULL,
  `ket_file` varbinary(50) DEFAULT NULL,
  `nidn` char(15) DEFAULT NULL,
  PRIMARY KEY (`kd_upload`),
  KEY `rep` (`kd_mk`),
  KEY `dosup` (`nidn`),
  CONSTRAINT `dosup` FOREIGN KEY (`nidn`) REFERENCES `tbl_dosen` (`nidn`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rep` FOREIGN KEY (`kd_mk`) REFERENCES `tbl_mk` (`kd_mk`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_upload` */

insert  into `tbl_upload`(`kd_upload`,`kd_mk`,`tgl_upload`,`nama_file`,`ket_file`,`nidn`) values 
('UP-0000001','IPA','2018-08-01','surat-1245.pdf','Pertemuan I','01012008'),
('UP-0000002','MTK01','2018-08-10','jbptunikompp-gdl-angganurak-34479-8-unikom_a-i.pdf','Keterangan','01012008');

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id` varchar(25) NOT NULL,
  `username` varchar(50) NOT NULL,
  `status` enum('admin','mhs','dsn','pim') NOT NULL DEFAULT 'mhs',
  `password` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`id`,`username`,`status`,`password`) values 
('adm001','admin','admin','admin'),
('dsn001','Dosen','dsn','dosen'),
('mhs001','mahasiswa','mhs','mhs'),
('pim001','Pimpinan','pim','pim');

/* Trigger structure for table `tbl_mhs` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `af_in_mhs` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `af_in_mhs` AFTER INSERT ON `tbl_mhs` FOR EACH ROW BEGIN
	INSERT INTO tbl_user VALUES (new.nim,new.nama_mhs,'mhs',new.nim);
    END */$$


DELIMITER ;

/*Table structure for table `vw_peserta` */

DROP TABLE IF EXISTS `vw_peserta`;

/*!50001 DROP VIEW IF EXISTS `vw_peserta` */;
/*!50001 DROP TABLE IF EXISTS `vw_peserta` */;

/*!50001 CREATE TABLE  `vw_peserta`(
 `no_peserta` char(10) ,
 `nama_peserta` varchar(50) ,
 `tempat_lahir` varchar(50) ,
 `ttl` varchar(50) ,
 `jk_peserta` enum('L','P') ,
 `alamat_peserta` varchar(100) ,
 `no_hp` char(15) ,
 `no_reg` char(10) ,
 `tgl_reg` date ,
 `biaya_reg` int(11) 
)*/;

/*Table structure for table `vw_upload` */

DROP TABLE IF EXISTS `vw_upload`;

/*!50001 DROP VIEW IF EXISTS `vw_upload` */;
/*!50001 DROP TABLE IF EXISTS `vw_upload` */;

/*!50001 CREATE TABLE  `vw_upload`(
 `kd_upload` varchar(10) ,
 `kd_mk` char(10) ,
 `tgl_upload` date ,
 `nama_file` varchar(100) ,
 `ket_file` varbinary(50) ,
 `nidn` char(15) ,
 `nama_dosen` varchar(30) ,
 `nama_mk` varchar(100) 
)*/;

/*View structure for view vw_peserta */

/*!50001 DROP TABLE IF EXISTS `vw_peserta` */;
/*!50001 DROP VIEW IF EXISTS `vw_peserta` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_peserta` AS select `tbl_peserta`.`no_peserta` AS `no_peserta`,`tbl_peserta`.`nama_peserta` AS `nama_peserta`,`tbl_peserta`.`tempat_lahir` AS `tempat_lahir`,`tbl_peserta`.`ttl` AS `ttl`,`tbl_peserta`.`jk_peserta` AS `jk_peserta`,`tbl_peserta`.`alamat_peserta` AS `alamat_peserta`,`tbl_peserta`.`no_hp` AS `no_hp`,`tbl_peserta`.`no_reg` AS `no_reg`,`tbl_registrasi`.`tgl_reg` AS `tgl_reg`,`tbl_peserta`.`biaya_reg` AS `biaya_reg` from (`tbl_peserta` join `tbl_registrasi` on((`tbl_peserta`.`no_reg` = `tbl_registrasi`.`no_reg`))) */;

/*View structure for view vw_upload */

/*!50001 DROP TABLE IF EXISTS `vw_upload` */;
/*!50001 DROP VIEW IF EXISTS `vw_upload` */;

/*!50001 CREATE ALGORITHM=TEMPTABLE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_upload` AS select `tbl_upload`.`kd_upload` AS `kd_upload`,`tbl_upload`.`kd_mk` AS `kd_mk`,`tbl_upload`.`tgl_upload` AS `tgl_upload`,`tbl_upload`.`nama_file` AS `nama_file`,`tbl_upload`.`ket_file` AS `ket_file`,`tbl_upload`.`nidn` AS `nidn`,`tbl_dosen`.`nama_dosen` AS `nama_dosen`,`tbl_mk`.`nama_mk` AS `nama_mk` from ((`tbl_upload` join `tbl_mk` on((`tbl_upload`.`kd_mk` = `tbl_mk`.`kd_mk`))) join `tbl_dosen` on((`tbl_upload`.`nidn` = `tbl_dosen`.`nidn`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
